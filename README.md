# MSTS_InvoiceMe

## Requirements
- PHP 5.4 or higher
- php extension 'openssl'

## Installation

### Manual installation
- Copy all the files except `modman`, `LICENSE.txt` and `README.md` from the module archive to the Magento root folder
- Flush Magento's cache

### Using Modman
- Make sure you have [Modman](https://github.com/colinmollenhour/modman) installed
- Install the module
```
cd [magento root folder]
modman init
modman clone --copy https://gitlab.com/msts-public/plugins/invoiceme-magento1.git
```
- Flush Magento's cache

#### How to update
```
cd [magento root folder]
modman update invoiceme-magento1 --force --copy
```
- Flush Magento's cache

## License
[MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
