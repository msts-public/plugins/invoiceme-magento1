Event.observe(window, 'load', function(){
    var createdWebhooksInheritCheckbox = $('payment_msts_invoiceme_created_webhooks_inherit');
    if (createdWebhooksInheritCheckbox) {
        createdWebhooksInheritCheckbox.hide();
        var createdWebhooksInheritLabel = createdWebhooksInheritCheckbox.next('label');
        if (createdWebhooksInheritLabel) {
            createdWebhooksInheritLabel.hide();
        }
    }
});
