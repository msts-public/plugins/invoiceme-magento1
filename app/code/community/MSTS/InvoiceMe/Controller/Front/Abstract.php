<?php

abstract class MSTS_InvoiceMe_Controller_Front_Abstract extends Mage_Core_Controller_Front_Action
{
    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();

        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        if (!$helper->validateModuleFullyConfigured()) {
            $this->norouteAction();
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }

        if (!$helper->validateOpensslPhpExtensionLoaded()) {
            $debugData = ['module_validation_error' => 'Required PHP extension \'openssl\' was not loaded.'];
            $helper->debug($debugData);
            $this->norouteAction();
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return mixed
     */
    protected function getBuyerStatus($customer)
    {
        /** @var MSTS_InvoiceMe_Helper_Customer $invoiceMeCustomerHelper */
        $invoiceMeCustomerHelper = Mage::helper('msts_invoiceme/customer');
        return $invoiceMeCustomerHelper->getBuyerStatus($customer);
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     */
    protected function fetchAndUpdateBuyerData($customer)
    {
        try {
            /** @var MSTS_InvoiceMe_Helper_Customer $customerHelper */
            $customerHelper = Mage::helper('msts_invoiceme/customer');
            $customerHelper->fetchAndUpdateBuyerData($customer);
            $customer->save();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param int $attemptNumber
     * @return string
     * @throws MSTS_InvoiceMe_NotUniqueClientReferenceIdException
     * @throws Exception
     */
    protected function assignUniqueClientReferenceIdToCustomer($customer, $attemptNumber = 1)
    {
        try {
            /** @var MSTS_InvoiceMe_Helper_Data $helper */
            $helper = Mage::helper('msts_invoiceme');
            $clientReferenceId = $helper->generateUuidV4();
            $customer->setMstsImClientReferenceId($clientReferenceId);
            $customer->save();
        } catch (MSTS_InvoiceMe_NotUniqueClientReferenceIdException $e) {
            $attemptNumber++;
            if ($attemptNumber > 10) {
                throw new MSTS_InvoiceMe_NotUniqueClientReferenceIdException(
                    'Could not assign an unique client reference id to the customer.'
                );
            }

            return $this->assignUniqueClientReferenceIdToCustomer($customer, $attemptNumber);
        }

        return $clientReferenceId;
    }

    /**
     * Build request URL to InvoiceMe application form
     *
     * @param $failurePath
     * @param $successPath
     * @param bool $redirectOnError
     * @return null|string
     */
    protected function buildApplyForCreditUrl($failurePath, $successPath, $redirectOnError = false)
    {
        /** @var Mage_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');
        if (!$programUrl = $helper->getProgramUrl()) {
            $customerSession->addError($this->__('There was an error trying to apply for a credit.'));
            $debugData = [
                'module_validation_error' => 'Program URL cannot be empty.',
            ];
            $helper->debug($debugData);
            if ($redirectOnError) {
                $this->_redirect($failurePath);
            }

            return null;
        }

        $customer = $customerSession->getCustomer();
        $clientReferenceId = $customer->getMstsImClientReferenceId();
        if (!$clientReferenceId) {
            try {
                $clientReferenceId = $this->assignUniqueClientReferenceIdToCustomer($customer);
            } catch (Exception $e) {
                $debugData = [
                    'exception' => ['error' => $e->getMessage(), 'code' => $e->getCode()],
                ];
                $helper->debug($debugData);
                $customerSession->addError($this->__('There was an error trying to apply for a credit.'));
                if ($redirectOnError) {
                    $this->_redirect($failurePath);
                }

                return null;
            }
        }

        $applyForCreditUrl = $programUrl . 'apply'
            . '?client_reference_id=' . $clientReferenceId
            . '&ecommerce_url=' . Mage::getUrl($successPath, ['_secure' => true]);

        /** @var MSTS_InvoiceMe_Helper_Customer $customerHelper */
        $customerHelper = Mage::helper('msts_invoiceme/customer');
        $customerFirstTransactedDate = $customerHelper->getCustomerFirstTransactedDate($customer);
        if ($customerFirstTransactedDate) {
            $applyForCreditUrl .= '&first_transacted_date=' . $customerFirstTransactedDate;
            $customerTransactedTotal = $customerHelper->getCustomerTransactedTotal($customer);
            if ($customerTransactedTotal) {
                $applyForCreditUrl .= '&total_transacted_amount=' . $customerTransactedTotal;
            }
        }

        $customerCurrentTransactionAmount = $customerHelper->getCustomerCurrentTransactionAmount();
        if ($customerCurrentTransactionAmount) {
            $applyForCreditUrl .= '&current_transaction_amount=' . $customerCurrentTransactionAmount;
        }

        return $applyForCreditUrl;
    }

}
