<?php

class MSTS_InvoiceMe_ApiErrorResponseException extends Mage_Core_Exception
{
    /** @var string */
    protected $apiErrorCode;

    /**
     * @param string $apiErrorCode
     */
    public function setApiErrorCode($apiErrorCode)
    {
        $this->apiErrorCode = $apiErrorCode;
    }

    /**
     * @return string
     */
    public function getApiErrorCode()
    {
        return $this->apiErrorCode;
    }
}
