<?php

class MSTS_InvoiceMe_Helper_Customer extends Mage_Core_Helper_Abstract
{
    const BUYER_STATUS_ACTIVE               = 'Active';
    const BUYER_STATUS_CANCELLED            = 'Cancelled';
    const BUYER_STATUS_DECLINED             = 'Declined';
    const BUYER_STATUS_INACTIVE             = 'Inactive';
    const BUYER_STATUS_PENDING              = 'Pending';
    const BUYER_STATUS_PENDING_DIRECT_DEBIT = 'Pending Direct Debit';
    const BUYER_STATUS_PENDING_SETUP        = 'Pending Setup';
    const BUYER_STATUS_SUSPENDED            = 'Suspended';

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return bool|false|string
     */
    public function getCustomerFirstTransactedDate($customer)
    {
        /** @var Mage_Sales_Model_Resource_Order_Collection $orderCollection */
        $orderCollection = Mage::getResourceModel('sales/order_collection');
        $orderCollection
            ->addAttributeToFilter('status', Mage_Sales_Model_Order::STATE_COMPLETE)
            ->addAttributeToFilter('customer_id', $customer->getId())
            ->addAttributeToSelect('created_at')
            ->setOrder('created_at', 'asc')
            ->setPageSize(1);

        if ($orderCollection->getSize() > 0) {
            $firstOrder = $orderCollection->getFirstItem();
            $firstOrderCreatedAtTimestamp = Mage::app()->getLocale()
                ->date($firstOrder->getCreatedAt(), Varien_Date::DATETIME_INTERNAL_FORMAT)->getTimestamp();
            return date('Y-m-d', $firstOrderCreatedAtTimestamp);
        }

        return false;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return float
     */
    public function getCustomerTransactedTotal($customer)
    {
        /** @var Mage_Sales_Model_Resource_Order_Collection $orderCollection */
        $orderCollection = Mage::getResourceModel('sales/order_collection');

        $orderTotals = $orderCollection
            ->addAttributeToFilter('status', Mage_Sales_Model_Order::STATE_COMPLETE)
            ->addAttributeToFilter('customer_id', $customer->getId())
            ->addAttributeToSelect('grand_total')
            ->getColumnValues('grand_total');

        return round(array_sum($orderTotals) * 100);
    }

    /**
     * @return bool|float
     */
    public function getCustomerCurrentTransactionAmount()
    {
        /** @var Mage_Checkout_Helper_Cart $cartHelper */
        $cartHelper = Mage::helper('checkout/cart');
        $quoteBaseGrandTotal = $cartHelper->getQuote()->getBaseGrandTotal();
        if ($quoteBaseGrandTotal > 0) {
            return round($quoteBaseGrandTotal * 100);
        }

        return false;
    }

    /**
     * Load customer by Client Reference Id
     *
     * @throws Mage_Core_Exception
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param string $clientReferenceId
     * @return Mage_Customer_Model_Customer
     */
    public function loadCustomerByClientReferenceId(Mage_Customer_Model_Customer $customer, $clientReferenceId)
    {
        $adapter = Mage::getSingleton('core/resource')->getConnection('customer_read');
        $bind    = ['client_reference_id' => $clientReferenceId];
        $select  = $adapter->select()
            ->from($customer->getResource()->getEntityTable(), [$customer->getResource()->getEntityIdField()])
            ->where('msts_im_client_reference_id = :client_reference_id');

        if ($customer->getSharingConfig()->isWebsiteScope()) {
            if (!$customer->hasData('website_id')) {
                Mage::throwException(
                    Mage::helper('customer')->__('Customer website ID must be specified when using the website scope')
                );
            }

            $bind['website_id'] = (int)$customer->getWebsiteId();
            $select->where('website_id = :website_id');
        }

        $customerId = $adapter->fetchOne($select, $bind);
        if ($customerId) {
            $customer->getResource()->load($customer, $customerId);
        } else {
            $customer->setData([]);
        }

        return $customer;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return mixed
     */
    public function getBuyerStatus($customer)
    {
        return Mage::getResourceSingleton('customer/customer')
            ->getAttribute('msts_im_status')
            ->getSource()
            ->getOptionText($customer->getMstsImStatus());
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @throws Mage_Core_Exception
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     */
    public function fetchAndUpdateBuyerData($customer)
    {
        $api = $this->getApi();
        $api->setPathIdParam(Mage::getModel('core/encryption')->decrypt($customer->getMstsImBuyerId()));
        $api->callGetsTheStatusAndCreditInformationForABuyer();

        $this->updateCustomerData($customer, $api);
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param MSTS_InvoiceMe_Model_Api_InvoiceMe $api
     */
    protected function updateCustomerData(Mage_Customer_Model_Customer $customer, $api)
    {
        $customer->setMstsImBusinessName($api->getBusinessName());
        $statusOptionId = Mage::getResourceSingleton('customer/customer')
            ->getAttribute('msts_im_status')
            ->getSource()
            ->getOptionId($api->getStatus());
        $customer->setMstsImStatus($statusOptionId);
        $currency = $api->getCurrency();
        if (is_array($currency)) {
            $currency = reset($currency);
        }

        $customer->setMstsImCurrency($currency);
        $customer->setMstsImCreditApproved($api->getCreditApproved());
        $customer->setMstsImCreditBalance($api->getCreditBalance());
        $customer->setMstsImCreditPreauthorized($api->getCreditPreauthorized());
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     */
    protected function getApi()
    {
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');
        return $helper->getApi();
    }
}
