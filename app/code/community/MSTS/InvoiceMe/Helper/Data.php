<?php

class MSTS_InvoiceMe_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ORDER_STATUS_PENDING_INVOICEME                    = 'pending_invoiceme';

    const XML_PATH_API_KEY                                  = 'api_key';
    const XML_PATH_API_KEY_FOR_CREATED_WEBHOOKS             = 'api_key_for_created_webhooks';
    const XML_PATH_API_URL                                  = 'api_url';
    const XML_PATH_AVAILABILITY                             = 'availability';
    const XML_PATH_BASE_URL_FOR_CREATED_WEBHOOKS            = 'base_url_for_created_webhooks';
    const XML_PATH_CREATED_WEBHOOKS                         = 'created_webhooks';
    const XML_PATH_ACTIVE                                   = 'active';
    const XML_PATH_PAYMENT_ACTION                           = 'payment_action';
    const XML_PATH_DEBUG                                    = 'debug';
    const XML_PATH_PROGRAM_URL                              = 'program_url';
    const XML_PATH_SANDBOX                                  = 'sandbox';
    const XML_PATH_SANDBOX_API_KEY                          = 'sandbox_api_key';
    const XML_PATH_SANDBOX_API_URL                          = 'sandbox_api_url';
    const XML_PATH_SANDBOX_PROGRAM_URL                      = 'sandbox_program_url';
    const XML_PATH_SANDBOX_SELLER_ID                        = 'sandbox_seller_id';
    const XML_PATH_SELLER_ID                                = 'seller_id';
    const XML_PATH_SUPPORT_MUMBER                           = 'support_number';
    const XML_PATH_WEBHOOK_AUTH_TOKEN_FOR_CREATED_WEBHOOKS  = 'webhook_auth_token_for_created_webhooks';

    /** @var null|string */
    protected $scope = null;
    /** @var int */
    protected $scopeId = 0;
    /** @var null|string */
    protected $scopeCode = null;

    /** @var array */
    protected $currenciesWithThreeDecimalPlaces = [
        'BHD',
        'IQD',
        'JOD',
        'KWD',
        'OMR',
        'TND',
    ];
    /** @var array */
    protected $currenciesWithZeroDecimalPlaces = [
        'BIF',
        'BYR',
        'CLP',
        'DJF',
        'GNF',
        'GWP',
        'JPY',
        'KMF',
        'KRW',
        'MGA',
        'PYG',
        'RWF',
        'VND',
        'VUV',
        'XAF',
        'XOF',
        'XPF',
    ];

    /**
     * @param string $scope
     * @return MSTS_InvoiceMe_Helper_Data
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

    /**
     * @param int $scopeId
     * @return MSTS_InvoiceMe_Helper_Data
     */
    public function setScopeId($scopeId)
    {
        $this->scopeId = $scopeId;
        return $this;
    }

    /**
     * @param null|string $scopeCode
     * @return MSTS_InvoiceMe_Helper_Data
     */
    public function setScopeCode($scopeCode)
    {
        $this->scopeCode = $scopeCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isActive()
    {
        return $this->getConfigData(self::XML_PATH_ACTIVE);
    }

    /**
     * @return mixed
     */
    public function isDebug()
    {
        return $this->getConfigData(self::XML_PATH_DEBUG);
    }

    /**
     * @return mixed
     */
    public function getApiUrl()
    {
        if ($this->isSandboxEnabled()) {
            return $this->getConfigData(self::XML_PATH_SANDBOX_API_URL);
        }

        return $this->getConfigData(self::XML_PATH_API_URL);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        if ($this->isSandboxEnabled()) {
            return $this->getConfigData(self::XML_PATH_SANDBOX_API_KEY);
        }

        return $this->getConfigData(self::XML_PATH_API_KEY);
    }

    /**
     * @return mixed
     */
    public function getPaymentAction()
    {
        return $this->getConfigData(self::XML_PATH_PAYMENT_ACTION);
    }

    /**
     * @return mixed
     */
    public function isSandboxEnabled()
    {
        return $this->getConfigData(self::XML_PATH_SANDBOX);
    }

    /**
     * @return mixed
     */
    public function getCreatedWebhooks()
    {
        return $this->getConfigData(self::XML_PATH_CREATED_WEBHOOKS);
    }

    /**
     * @return mixed
     */
    public function getApiKeyForCreatedWebhooks()
    {
        return $this->getConfigData(self::XML_PATH_API_KEY_FOR_CREATED_WEBHOOKS);
    }

    /**
     * @return mixed
     */
    public function getBaseUrlForCreatedWebhooks()
    {
        return $this->getConfigData(self::XML_PATH_BASE_URL_FOR_CREATED_WEBHOOKS);
    }

    /**
     * @return mixed
     */
    public function getAvailability()
    {
        return $this->getConfigData(self::XML_PATH_AVAILABILITY);
    }

    /**
     * @return mixed
     */
    public function getSupportNumber()
    {
        return $this->getConfigData(self::XML_PATH_SUPPORT_MUMBER);
    }

    /**
     * @return mixed
     */
    public function getWebhookAuthTokenForCreatedWebhooks()
    {
        return $this->getConfigData(self::XML_PATH_WEBHOOK_AUTH_TOKEN_FOR_CREATED_WEBHOOKS);
    }

    /**
     * @return mixed
     */
    public function getProgramUrl()
    {
        if ($this->isSandboxEnabled()) {
            return $this->getConfigData(self::XML_PATH_SANDBOX_PROGRAM_URL);
        }

        return $this->getConfigData(self::XML_PATH_PROGRAM_URL);
    }

    /**
     * @return mixed
     */
    public function getSellerId()
    {
        if ($this->isSandboxEnabled()) {
            return $this->getConfigData(self::XML_PATH_SANDBOX_SELLER_ID);
        }

        return $this->getConfigData(self::XML_PATH_SELLER_ID);
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     */
    public function getApi()
    {
        return Mage::getModel('msts_invoiceme/api_invoiceMe');
    }

    /**
     * @return string
     */
    public function getBaseSecureUrl()
    {
        $url = (string)Mage::getConfig()->getNode(
            Mage_Core_Model_Url::XML_PATH_SECURE_URL,
            $this->scope,
            $this->scopeCode
        );
        if (false !== strpos($url, '{{base_url}}')) {
            $baseUrl = Mage::getConfig()->substDistroServerVars('{{base_url}}');
            $url = str_replace('{{base_url}}', $baseUrl, $url);
        }

        return $url;
    }

    /**
     * @return bool
     */
    public function validateModuleFullyConfigured()
    {
        if (!$this->isActive()
            || !$this->getApiUrl()
            || !$this->getApiKey()
            || !$this->getSellerId()
            || !$this->getProgramUrl()
            || !Mage::getModel('msts_invoiceme/webhook')->validateAllWebhooksCreated(
                Mage::helper('core')->jsonDecode($this->getCreatedWebhooks())
            )
            || !$this->validateApiKeyForCreatedWebhooks()
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function validateApiKeyForCreatedWebhooks()
    {
        $apiKeyForCreatedWebhooks = $this->getApiKeyForCreatedWebhooks();
        return $apiKeyForCreatedWebhooks === $this->getApiKey();
    }

    /**
     * @return bool
     */
    public function validateBaseUrlForCreatedWebhooks()
    {
        $baseUrlForCreatedWebhooks = $this->getBaseUrlForCreatedWebhooks();
        return $baseUrlForCreatedWebhooks === $this->getBaseSecureUrl();
    }

    /**
     * @return bool
     */
    public function validateOpensslPhpExtensionLoaded()
    {
        return extension_loaded('openssl');
    }

    /**
     * @return string
     */
    public function generateUuidV4()
    {
        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100 (RCF 4122 - Section 4.1.3)
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10 (RCF 4122 - Section 4.4)
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * @param float $value
     * @param string $currency
     * @return int
     */
    public function amountToSubunits($value, $currency)
    {
        return round($value * $this->getMultiplier($currency));
    }

    /**
     * @param int|string $value
     * @param string $currency
     * @return float
     */
    public function amountFromSubunits($value, $currency)
    {
        return floatval(sprintf('%.2F', round($value) / $this->getMultiplier($currency)));
    }

    /**
     * @param string $currency
     * @return int
     */
    protected function getMultiplier($currency)
    {
        $multiplier = 100;
        if (in_array($currency, $this->currenciesWithThreeDecimalPlaces)) {
            $multiplier = 1000;
        } elseif (in_array($currency, $this->currenciesWithZeroDecimalPlaces)) {
            $multiplier = 1;
        }

        return $multiplier;
    }

    /**
     * @param string $value
     * @return string
     */
    public function maskValue($value)
    {
        $length = strlen($value);
        if ($length > 8) {
            $length = 8;
        }

        return substr($value, 0, $length) . str_repeat('*', $length);
    }

    /**
     * @param array $webhooksData
     */
    public function updateCreatedWebhooks($webhooksData)
    {
        $apiKeyForCreatedWebhooks = $this->updateApiKeyForCreatedWebhooks();
        $baseUrlForCreatedWebhooks = $this->updateBaseUrlForCreatedWebhooks();
        $webhookAuthTokenForCreatedWebhooks = $this->updateWebhookAuthTokenForCreatedWebhooks($webhooksData);

        Mage::getConfig()->saveConfig(
            $this->getConfigPath(self::XML_PATH_CREATED_WEBHOOKS),
            Mage::helper('core')->jsonEncode($webhooksData),
            $this->scope,
            $this->scopeId
        );

        $this->updateCreatedWebhooksInOtherScopesForTheSameApiKey(
            $webhooksData,
            $apiKeyForCreatedWebhooks,
            $baseUrlForCreatedWebhooks,
            $webhookAuthTokenForCreatedWebhooks
        );
    }

    /**
     * @return string
     */
    protected function updateBaseUrlForCreatedWebhooks()
    {
        $baseUrlForCreatedWebhooks = $this->getBaseSecureUrl();

        Mage::getConfig()->saveConfig(
            $this->getConfigPath(self::XML_PATH_BASE_URL_FOR_CREATED_WEBHOOKS),
            $baseUrlForCreatedWebhooks,
            $this->scope,
            $this->scopeId
        );
        return $baseUrlForCreatedWebhooks;
    }

    /**
     * @return string
     */
    protected function updateApiKeyForCreatedWebhooks()
    {
        $apiKeyPath = $this->getConfigPath(self::XML_PATH_API_KEY);
        if ($this->isSandboxEnabled()) {
            $apiKeyPath = $this->getConfigPath(self::XML_PATH_SANDBOX_API_KEY);
        }

        $apiKey = (string)Mage::getConfig()->getNode($apiKeyPath, $this->scope, $this->scopeCode);

        Mage::getConfig()->saveConfig(
            $this->getConfigPath(self::XML_PATH_API_KEY_FOR_CREATED_WEBHOOKS),
            $apiKey,
            $this->scope,
            $this->scopeId
        );
        return $apiKey;
    }

    /**
     * @param array $webhooksData
     * @return string
     */
    protected function updateWebhookAuthTokenForCreatedWebhooks($webhooksData)
    {
        $webhookAuthTokenForCreatedWebhooks = $webhookAuthToken = '';
        if (is_array($webhooksData)) {
            foreach ($webhooksData as $index => $webhookData) {
                if (is_array($webhookData)) {
                    if (isset($webhookData['webhook_auth_token'])) {
                        $webhookAuthToken = $webhookData['webhook_auth_token'];
                        break;
                    }
                } else {
                    if ('webhook_auth_token' == $index) {
                        $webhookAuthToken = $webhookData;
                        break;
                    }
                }
            }
        }

        if ('' != $webhookAuthToken) {
            $webhookAuthTokenForCreatedWebhooks = Mage::getModel('core/encryption')->encrypt($webhookAuthToken);
        }

        Mage::getConfig()->saveConfig(
            $this->getConfigPath(self::XML_PATH_WEBHOOK_AUTH_TOKEN_FOR_CREATED_WEBHOOKS),
            $webhookAuthTokenForCreatedWebhooks,
            $this->scope,
            $this->scopeId
        );

        return $webhookAuthTokenForCreatedWebhooks;
    }

    /**
     * @param array $webhooksData
     * @param string $apiKeyForCreatedWebhooks
     * @param string $baseUrlForCreatedWebhooks
     * @param string $webhookAuthTokenForCreatedWebhooks
     */
    protected function updateCreatedWebhooksInOtherScopesForTheSameApiKey(
        $webhooksData,
        $apiKeyForCreatedWebhooks,
        $baseUrlForCreatedWebhooks,
        $webhookAuthTokenForCreatedWebhooks
    ) {
        if ('default' === $this->scope) {
            foreach (Mage::app()->getWebsites() as $website) {
                $this->updateCreatedWebhooksForTheSameApiKey(
                    $webhooksData,
                    $apiKeyForCreatedWebhooks,
                    $baseUrlForCreatedWebhooks,
                    $webhookAuthTokenForCreatedWebhooks,
                    'websites',
                    $website->getId()
                );
            }
        } elseif ('websites' == $this->scope) {
            $this->updateCreatedWebhooksForTheSameApiKey(
                $webhooksData,
                $apiKeyForCreatedWebhooks,
                $baseUrlForCreatedWebhooks,
                $webhookAuthTokenForCreatedWebhooks,
                'default',
                0
            );

            foreach (Mage::app()->getWebsites() as $website) {
                if ($this->scopeId === $website->getId()) {
                    continue;
                }

                $this->updateCreatedWebhooksForTheSameApiKey(
                    $webhooksData,
                    $apiKeyForCreatedWebhooks,
                    $baseUrlForCreatedWebhooks,
                    $webhookAuthTokenForCreatedWebhooks,
                    'websites',
                    $website->getId()
                );
            }
        }
    }

    /**
     * @param array $webhooksData
     * @param string $apiKeyForCreatedWebhooks
     * @param string $baseUrlForCreatedWebhooks
     * @param string $webhookAuthTokenForCreatedWebhooks
     * @param null|string $scope
     * @param int $scopeId
     */
    protected function updateCreatedWebhooksForTheSameApiKey(
        $webhooksData,
        $apiKeyForCreatedWebhooks,
        $baseUrlForCreatedWebhooks,
        $webhookAuthTokenForCreatedWebhooks,
        $scope,
        $scopeId
    ) {
        $row = $this->getConfigDataConfiguredDirectlyInTheScope(
            $this->getConfigPath(self::XML_PATH_API_KEY_FOR_CREATED_WEBHOOKS),
            $scope,
            $scopeId
        );

        if ($row && isset($row['value'])) {
            $apiKeyForCreatedWebhooksAtTheGivenScope = $row['value'];
            if ($apiKeyForCreatedWebhooksAtTheGivenScope == $apiKeyForCreatedWebhooks) {
                $this->updateConfigDataConfiguredDirectlyInTheScopeIfExists(
                    $this->getConfigPath(self::XML_PATH_CREATED_WEBHOOKS),
                    Mage::helper('core')->jsonEncode($webhooksData),
                    $scope,
                    $scopeId
                );

                $this->updateConfigDataConfiguredDirectlyInTheScopeIfExists(
                    $this->getConfigPath(self::XML_PATH_WEBHOOK_AUTH_TOKEN_FOR_CREATED_WEBHOOKS),
                    $webhookAuthTokenForCreatedWebhooks,
                    $scope,
                    $scopeId
                );

                $this->updateConfigDataConfiguredDirectlyInTheScopeIfExists(
                    $this->getConfigPath(self::XML_PATH_BASE_URL_FOR_CREATED_WEBHOOKS),
                    $baseUrlForCreatedWebhooks,
                    $scope,
                    $scopeId
                );
            }
        }
    }

    /**
     * @param string $path
     * @param null|string $scope
     * @param int $scopeId
     * @return array
     */
    protected function getConfigDataConfiguredDirectlyInTheScope($path, $scope, $scopeId)
    {
        /** @var Magento_Db_Adapter_Pdo_Mysql $adapter */
        $adapter = Mage::getSingleton('core/resource')->getConnection('read');

        $select = $adapter->select()
            ->from($adapter->getTableName('core_config_data'))
            ->where('path = ?', $path)
            ->where('scope = ?', $scope)
            ->where('scope_id = ?', $scopeId);
        return $adapter->fetchRow($select);
    }

    /**
     * @param string $path
     * @param string $value
     * @param null|string $scope
     * @param int $scopeId
     */
    protected function updateConfigDataConfiguredDirectlyInTheScopeIfExists($path, $value, $scope, $scopeId)
    {
        $row = $this->getConfigDataConfiguredDirectlyInTheScope(
            $path,
            $scope,
            $scopeId
        );

        if ($row && isset($row['config_id'])) {
            Mage::getConfig()->saveConfig(
                $path,
                $value,
                $scope,
                $scopeId
            );
        }
    }

    /**
     * @param string $field
     * @return string|null
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function getConfigData($field)
    {
        $path = $this->getConfigPath($field);
        if (null === $this->scope) {
            $storeId = Mage::app()->getStore()->getId();
            return Mage::getStoreConfig($path, $storeId);
        }

        return $this->processConfigValue(Mage::getConfig()->getNode($path, $this->scope, $this->scopeCode), $path);
    }

    /**
     * @param string $field
     * @return string
     */
    protected function getConfigPath($field)
    {
        return 'payment/msts_invoiceme/' . $field;
    }

    /**
     * @param Mage_Core_Model_Config_Element $node
     * @param string $path
     * @return string
     */
    protected function processConfigValue($node, $path)
    {
        $value = (string)$node;
        if (!empty($node['backend_model']) && !empty($value)) {
            $backend = Mage::getModel((string)$node['backend_model']);
            $backend->setPath($path)->setValue($value)->afterLoad();
            $value = $backend->getValue();
        }

        return $value;
    }

    /**
     * @param mixed $debugData
     */
    public function debug($debugData)
    {
        if ($this->isDebug()) {
            Mage::getModel(
                'core/log_adapter',
                'payment_' . MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE . '.log'
            )->log($debugData);
        }
    }
}
