<?php

$status = Mage::getModel('sales/order_status')->load(MSTS_InvoiceMe_Helper_Data::ORDER_STATUS_PENDING_INVOICEME);
if (!$status->getStatus()) {
    $status->setStatus(MSTS_InvoiceMe_Helper_Data::ORDER_STATUS_PENDING_INVOICEME);
    $status->setLabel(Mage::helper('msts_invoiceme')->__('Pending InvoiceMe'));
    $status->save();
}
