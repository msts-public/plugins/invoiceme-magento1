<?php

class MSTS_InvoiceMe_Block_Payment_Info_InvoiceMe extends Mage_Payment_Block_Info
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('msts/invoiceme/payment/info/invoiceme.phtml');
    }
}