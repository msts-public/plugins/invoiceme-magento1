<?php

class MSTS_InvoiceMe_Block_Payment_Form_InvoiceMe extends Mage_Payment_Block_Form
{

    /** @var mixed */
    protected $buyerStatus = null;

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('msts/invoiceme/payment/form/invoiceme.phtml');
    }

    /**
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return Mage::getSingleton('customer/session')->isLoggedIn();

    }

    /**
     * @return string
     */
    public function getCustomerLoginPageUrl()
    {
        return Mage::helper('customer')->getLoginUrl();
    }

    /**
     * @param bool $isAdminCheckout
     * @return mixed
     */
    public function getBuyerStatus($isAdminCheckout = false)
    {
        if (null === $this->buyerStatus) {
            if ($isAdminCheckout) {
                /** @var Mage_Adminhtml_Model_Session_Quote $quoteSession */
                $quoteSession = Mage::getSingleton('adminhtml/session_quote');
                $customer = $quoteSession->getCustomer();
            } else {
                /** @var Mage_Customer_Model_Session $customerSession */
                $customerSession = Mage::getSingleton('customer/session');
                $customer = $customerSession->getCustomer();
            }

            /** @var MSTS_InvoiceMe_Helper_Customer $invoiceMeCustomerHelper */
            $invoiceMeCustomerHelper = Mage::helper('msts_invoiceme/customer');
            $this->buyerStatus = $invoiceMeCustomerHelper->getBuyerStatus($customer);
        }

        return $this->buyerStatus;
    }

    /**
     * @return string
     */
    public function getInvoiceMeUrl()
    {
        return Mage::getUrl('msts_invoiceme');
    }
}
