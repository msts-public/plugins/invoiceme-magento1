<?php

class MSTS_InvoiceMe_Block_Customer_InvoiceMeLink extends Mage_Core_Block_Template
{
    public function addInvoiceMeLink()
    {
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        if (($parentBlock = $this->getParentBlock()) && $helper->validateModuleFullyConfigured()) {
            /** @var Mage_Customer_Block_Account_Navigation $parentBlock */
            $parentBlock->addLink('msts_invoiceme', 'msts_invoiceme', $this->__('InvoiceMe'));
        }
    }
}
