<?php

class MSTS_InvoiceMe_Block_Customer_InvoiceMe extends Mage_Customer_Block_Account_Dashboard
{
    /** @var mixed */
    protected $buyerStatus = null;

    /** @var array */
    protected $messageOnlyStatuses = [
        MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_CANCELLED,
        MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_DECLINED,
        MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_INACTIVE,
        MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING,
        MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING_DIRECT_DEBIT,
        MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING_SETUP,
        MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_SUSPENDED,
    ];

    /**
     * @return mixed
     */
    public function getBuyerStatus()
    {
        if (null === $this->buyerStatus) {
            /** @var Mage_Customer_Model_Session $customerSession */
            $customerSession = Mage::getSingleton('customer/session');
            $customer = $customerSession->getCustomer();

            /** @var MSTS_InvoiceMe_Helper_Customer $invoiceMeCustomerHelper */
            $invoiceMeCustomerHelper = Mage::helper('msts_invoiceme/customer');
            $this->buyerStatus = $invoiceMeCustomerHelper->getBuyerStatus($customer);
        }

        return $this->buyerStatus;
    }

    /**
     * @return bool
     */
    public function shouldDisplayMessageOnly()
    {
        return in_array($this->getBuyerStatus(), $this->messageOnlyStatuses);
    }

    /**
     * @return mixed
     */
    public function getProgramUrl()
    {
        return Mage::helper('msts_invoiceme')->getProgramUrl();
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        $programUrl = $this->getProgramUrl();
        $message = '';
        switch ($this->buyerStatus) {
            case MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_CANCELLED:
                $message = $this->__('Sorry, for some reason your application has been cancelled. Please visit <a href="%s">%s</a> to resubmit.', $programUrl, $programUrl);
                break;
            case MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_DECLINED:
                $storeName = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
                $message = $this->__('Sorry, your credit application has been denied. Please use another payment method for business purchases from %s.', $storeName);
                break;
            case MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_INACTIVE:
                $message = $this->__('Oh no! Your InvoiceMe account is inactive. This can be for many reasons, please visit <a href="%s">%s</a> to resolve this matter.', $programUrl, $programUrl);
                break;
            case MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING:
                $message = $this->__('We are currently reviewing your enrollment application for InvoiceMe. This process normally takes around 4 business hours to complete. If we have any questions or an approval, we will reach out to you via email.');
                break;
            case MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING_DIRECT_DEBIT:
            case MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING_SETUP:
                $message = $this->__('You have been approved to make purchases on terms! To activate your InvoiceMe account, please visit <a href="%s">%s</a> to complete your setup.', $programUrl, $programUrl);
                break;
            case MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_SUSPENDED:
                $message = $this->__('Whoops! Your InvoiceMe account has been suspended. This is likely due to past due payments or needing a credit line increase. Please visit <a href="%s">%s</a> to resolve this matter.', $programUrl, $programUrl);
                break;
        }

        return $message;
    }

    /**
     * @param string $price
     * @return string
     * @throws Zend_Currency_Exception
     */
    public function getPriceFormatted($price)
    {
        $currency = $this->getCustomer()->getMstsImCurrency();
        return Mage::app()->getLocale()->currency($currency)->toCurrency($price);
    }

    /**
     * @return string
     */
    public function getBuyerPortalURL()
    {
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');
        if (!$programUrl = $helper->getProgramUrl()) {
            $debugData = [
                'module_validation_error' => 'Program URL cannot be empty.',
            ];
            $helper->debug($debugData);
            return '#';
        }

        return $programUrl;
    }

    /**
     * @return bool
     */
    public function isInvoiceMeOrder()
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::registry('current_order');
        $invoiceMeOrder = false;
        if ($order && $order->getId()) {
            if (MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE == $order->getPayment()->getMethod()) {
                $invoiceMeOrder = true;
            }
        }

        return $invoiceMeOrder;
    }
}
