<?php

abstract class MSTS_InvoiceMe_Block_Adminhtml_Sales_Order_DisableFeaturesAbstract extends Mage_Adminhtml_Block_Template
{
    /**
     * @return string
     */
    abstract public function getJsFunctionName();

    /**
     * @return bool
     */
    protected function isInvoiceMeMethodUsedByOrder()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        if ($orderId) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order && $order->getId()
                && $order->getPayment()
                && $order->getPayment()->getMethod() == MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    protected function _toHtml()
    {
        if (!$this->isInvoiceMeMethodUsedByOrder()) {
            return '';
        }

        return parent::_toHtml();
    }

}
