<?php

class MSTS_InvoiceMe_Block_Adminhtml_Sales_Order_Creditmemo_HideRefundButton extends MSTS_InvoiceMe_Block_Adminhtml_Sales_Order_DisableFeaturesAbstract
{
    /**
     * @return string
     */
    public function getJsFunctionName()
    {
        /** @var Mage_Sales_Model_Order_Creditmemo $creditmemo */
        $creditmemo = Mage::registry('current_creditmemo');
        if (0 == $creditmemo->getBaseGrandTotal()) {
            return 'hideInvoicemeRefundOnline';
        } else {
            return 'hideInvoicemeRefundOffline';
        }
    }

    /**
     * @return string
     */
    public function getRefundButtonTitle()
    {
        /** @var Mage_Sales_Model_Order_Creditmemo $creditmemo */
        $creditmemo = Mage::registry('current_creditmemo');
        $refundButtonTitle = 'Refund Offline';
        if (0 == $creditmemo->getBaseGrandTotal()) {
            $refundButtonTitle = 'Refund';
        }

        return Mage::helper('sales')->__($refundButtonTitle);
    }

}
