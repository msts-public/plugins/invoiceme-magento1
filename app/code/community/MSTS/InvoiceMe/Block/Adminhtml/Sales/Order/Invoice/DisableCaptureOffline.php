<?php

class MSTS_InvoiceMe_Block_Adminhtml_Sales_Order_Invoice_DisableCaptureOffline extends MSTS_InvoiceMe_Block_Adminhtml_Sales_Order_DisableFeaturesAbstract
{
    /**
     * @return string
     */
    public function getJsFunctionName()
    {
        return 'disableInvoicemeCaptureOffline';
    }

}
