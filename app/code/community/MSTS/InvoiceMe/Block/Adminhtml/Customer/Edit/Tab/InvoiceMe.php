<?php

class MSTS_InvoiceMe_Block_Adminhtml_Customer_Edit_Tab_InvoiceMe extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Initialize form
     *
     * @return Mage_Adminhtml_Block_Customer_Edit_Tab_Account
     */
    public function initForm()
    {
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('_invoiceme');
        $form->setFieldNameSuffix('invoiceme');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend'    => $helper->__('InvoiceMe')
            ]
        );

        $fieldset->addField(
            'msts_im_business_name',
            'text',
            [
                'label'     => $helper->__('Business Name'),
                'readonly'  => true
            ]
        );

        $fieldset->addField(
            'msts_im_status',
            'text',
            [
                'label'     => $helper->__('Status'),
                'readonly'  => true
            ]
        );

        $fieldset->addField(
            'msts_im_buyer_id',
            'text',
            [
                'label'     => $helper->__('Buyer ID (first 8 characters)'),
                'readonly'  => true
            ]
        );

        $fieldset->addField(
            'msts_im_client_reference_id',
            'text',
            [
                'label'     => $helper->__('Client Reference ID'),
                'readonly'  => true
            ]
        );

        $fieldset->addField(
            'msts_im_currency',
            'text',
            [
                'label'     => $helper->__('Currency'),
                'readonly'  => true
            ]
        );

        $fieldset->addField(
            'msts_im_credit_approved',
            'text',
            [
                'label'     => $helper->__('Credit Approved'),
                'readonly'  => true
            ]
        );

        $fieldset->addField(
            'msts_im_credit_balance',
            'text',
            [
                'label'     => $helper->__('Credit Balance'),
                'readonly'  => true
            ]
        );

        $fieldset->addField(
            'msts_im_credit_preauthorized',
            'text',
            [
                'label'     => $helper->__('Credit Preauthorized'),
                'readonly'  => true
            ]
        );

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::registry('current_customer');
        /** @var MSTS_InvoiceMe_Helper_Customer $invoiceMeCustomerHelper */
        $invoiceMeCustomerHelper = Mage::helper('msts_invoiceme/customer');
        $form->setValues($customer->getData());
        $buyerId = substr(Mage::getModel('core/encryption')->decrypt($customer->getMstsImBuyerId()), 0, 8);
        $form->addValues(
            [
                'msts_im_status'                => $invoiceMeCustomerHelper->getBuyerStatus($customer),
                'msts_im_buyer_id'              => $buyerId,
                'msts_im_credit_approved'       => $this->formatPrice($customer->getMstsImCreditApproved()),
                'msts_im_credit_balance'        => $this->formatPrice($customer->getMstsImCreditBalance()),
                'msts_im_credit_preauthorized'  => $this->formatPrice($customer->getMstsImCreditPreauthorized()),
            ]
        );

        $this->setForm($form);
        return $this;
    }

    /**
     * @param $price
     * @return string
     */
    protected function formatPrice($price)
    {
        if (null === $price) {
            return '';
        }

        return number_format($price, 2, '.', '');
    }
}
