<?php

class MSTS_InvoiceMe_Block_Adminhtml_Customer_Edit_Tab_InvoiceMeLink extends Mage_Adminhtml_Block_Abstract
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * @return string
     */
    public function getTabClass()
    {
        return 'ajax';
    }

    /**
     * @return mixed
     */
    public function getTabUrl()
    {
        return $this->getUrl('*/customer_invoiceMe/index', ['_current' => true]);
    }

    /**
     * @return string
     */
    public function getAfter()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function getTabLabel()
    {
        return Mage::helper('msts_invoiceme')->__('InvoiceMe');
    }

    /**
     * @inheritdoc
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * @inheritdoc
     */
    public function canShowTab()
    {
        $customer = Mage::registry('current_customer');
        return (bool) $customer->getId();
    }

    /**
     * @inheritdoc
     */
    public function isHidden()
    {
        return false;
    }
}
