<?php

class MSTS_InvoiceMe_Block_Adminhtml_System_Config_Form_Button_Webhook_CheckCreated
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     * Set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('msts/invoiceme/system/config/form/button/checkCreatedWebhooks.phtml');
    }

    /**
     * Remove scope label
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for the "Check Created Webhooks" button
     *
     * @return string
     */
    public function getAjaxCheckCreatedWebhooksUrl()
    {
        /** @var Mage_Adminhtml_Model_Config_Data $configData */
        $configData = Mage::getSingleton('adminhtml/config_data');
        return Mage::getSingleton('adminhtml/url')->getUrl(
            '*/system_config_paymentMethods_invoiceMe/checkCreatedWebhooks',
            [
                'scope' => $configData->getScope(),
                'scopeId' => $configData->getScopeId(),
                'scopeCode' => $configData->getScopeCode(),
            ]
        );
    }

    /**
     * Generate Check Created Webhooks button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(
                [
                    'id'        => 'check_created_webhooks_button',
                    'label'     => $this->helper('msts_invoiceme')->__('Check Created Webhooks'),
                    'onclick'   => 'javascript:checkCreatedWebhooks(); return false;'
                ]
            );

        return $button->toHtml();
    }
}
