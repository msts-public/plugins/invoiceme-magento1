<?php

class MSTS_InvoiceMe_Block_Adminhtml_System_Config_Form_Field_Readonly extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     * @inheritdoc
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) 
    {
        $element->setDisabled('disabled');
        return parent::_getElementHtml($element);
    }
}
