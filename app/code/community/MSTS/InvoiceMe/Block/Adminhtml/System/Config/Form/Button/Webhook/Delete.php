<?php

class MSTS_InvoiceMe_Block_Adminhtml_System_Config_Form_Button_Webhook_Delete
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     * Set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('msts/invoiceme/system/config/form/button/deleteWebhooks.phtml');
    }

    /**
     * Remove scope label
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for the "Delete Webhooks" button
     *
     * @return string
     */
    public function getAjaxDeleteWebhooksUrl()
    {
        /** @var Mage_Adminhtml_Model_Config_Data $configData */
        $configData = Mage::getSingleton('adminhtml/config_data');
        return Mage::getSingleton('adminhtml/url')->getUrl(
            '*/system_config_paymentMethods_invoiceMe/deleteWebhooks',
            [
                'scope' => $configData->getScope(),
                'scopeId' => $configData->getScopeId(),
                'scopeCode' => $configData->getScopeCode(),
            ]
        );
    }

    /**
     * Generate "Delete Webhooks" button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(
                [
                    'id'        => 'delete_webhooks_button',
                    'label'     => $this->helper('msts_invoiceme')->__('Delete Webhooks'),
                    'onclick'   => 'javascript:deleteWebhooks(); return false;',
                    'class'     => 'delete'
                ]
            );

        return $button->toHtml();
    }
}
