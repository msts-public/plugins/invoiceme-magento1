<?php

class MSTS_InvoiceMe_Block_Adminhtml_System_Config_Form_Indicator_Webhook_Status
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     * Set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('msts/invoiceme/system/config/form/indicator.phtml');
    }

    /**
     * Remove scope label
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for the checkInvoiceMeWebhooksStatus javascript function
     *
     * @return string
     */
    public function getAjaxCheckWebhooksStatus()
    {
        /** @var Mage_Adminhtml_Model_Config_Data $configData */
        $configData = Mage::getSingleton('adminhtml/config_data');
        return Mage::getSingleton('adminhtml/url')->getUrl(
            '*/system_config_paymentMethods_invoiceMe/checkWebhooksStatus',
            [
                'scope' => $configData->getScope(),
                'scopeId' => $configData->getScopeId(),
                'scopeCode' => $configData->getScopeCode(),
            ]
        );
    }
}
