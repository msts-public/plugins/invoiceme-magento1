<?php

class MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe extends Mage_Payment_Model_Method_Abstract
{
    const METHOD_CODE = 'msts_invoiceme';

    const PREAUTHORIZATION_RESPONSE_STATUS_CANCELLED        = 'Cancelled';
    const PREAUTHORIZATION_RESPONSE_STATUS_CAPTURED         = 'Captured';
    const PREAUTHORIZATION_RESPONSE_STATUS_EXPIRED          = 'Expired';
    const PREAUTHORIZATION_RESPONSE_STATUS_PREAUTHORIZED    = 'Preauthorized';
    const PREAUTHORIZATION_RESPONSE_STATUS_REJECTED         = 'Rejected';

    const CHARGE_RESPONSE_STATUS_CANCELLED  = 'Cancelled';
    const CHARGE_RESPONSE_STATUS_CREATED    = 'Created';
    const CHARGE_RESPONSE_STATUS_PAID       = 'Paid';
    const CHARGE_RESPONSE_STATUS_REJECTED   = 'Rejected';

    const AVAILABILITY_ALL_CUSTOMERS        = 'all_customers';
    const AVAILABILITY_ACTIVE_BUYERS_ONLY   = 'active_buyers_only';

    const FORM_FIELD_NOTES      = 'invoiceme_notes';
    const FORM_FIELD_PO_NUMBER  = 'invoiceme_po_number';

    const FORM_FIELD_NOTES_MAXIMUM_LENGTH       = 1000;
    const FORM_FIELD_PO_NUMBER_MAXIMUM_LENGTH   = 200;

    const SHIP_TO_RECIPIENT_NAME_MAXIMUM_LENGTH     = 80;
    const SHIP_TO_COMPANY_NAME_MAXIMUM_LENGTH       = 80;
    const SHIP_TO_ADDRESS_LINE_MAXIMUM_LENGTH       = 30;
    const SHIP_TO_CITY_MAXIMUM_LENGTH               = 40;
    const SHIP_TO_STATE_MAXIMUM_LENGTH              = 10;
    const SHIP_TO_ZIP_MAXIMUM_LENGTH                = 15;
    const SHIP_TO_TRACKING_NUMBER_MAXIMUM_LENGTH    = 100;
    const SHIP_TO_TRACKING_URL_MAXIMUM_LENGTH       = 200;
    const SHIP_TO_CARRIER_MAXIMUM_LENGTH            = 200;

    protected $_code            = self::METHOD_CODE;
    protected $_formBlockType   = 'msts_invoiceme/payment_form_invoiceMe';
    protected $_infoBlockType   = 'msts_invoiceme/payment_info_invoiceMe';

    /**
     * Availability options
     */
    protected $_canAuthorize                = true;
    protected $_canCapture                  = true;
    protected $_canCapturePartial           = true;
    protected $_canVoid                     = true;
    protected $_canRefund                   = true;
    protected $_canRefundInvoicePartial     = true;

    protected $allowCurrencyCode = ['USD'];

    /**
     * Request object
     *
     * @var Zend_Controller_Request_Http
     */
    protected $request;

    /**
     * Used by Magento in Mage_Checkout_Model_Type_Onepage::saveOrder
     * to redirect a customer to the payment gateway
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl() 
    {
        try {
            $info = $this->getInfoInstance();
            if (is_object($info) && $quote = $info->getQuote()) {
                $customer = $quote->getCustomer();
                if ($customer && $customer->getId() && !$customer->getMstsImStatus()) {
                    return Mage::getUrl('msts_invoiceme/gateway/redirect', ['_secure' => true]);
                }
            }
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
        }

        return '';
    }

    /**
     * Send authorize request to API
     *
     * @param  Varien_Object $payment
     * @param  float $amount
     * @return MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe
     * @throws Mage_Core_Exception
     * @throws Exception
     */
    public function authorize(Varien_Object $payment, $amount)
    {
        /** @var Mage_Sales_Model_Order_Payment $payment */
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        if ($amount <= 0) {
            Mage::throwException($helper->__('Invalid amount for authorization.'));
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $payment->getOrder();
        if ($order->getCustomerIsGuest()) {
            Mage::throwException($helper->__('InvoiceMe payment method is available to registered customers only.'));
        }

        $buyerId = null;
        $customerStatus = null;
        $customer = $order->getCustomer();
        if ($customer && $customer->getId()) {
            $buyerId = $order->getCustomer()->getMstsImBuyerId();
            $customerStatus = $order->getCustomer()->getMstsImStatus();
        } else if ($order->getCustomerId()) {
            $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
            if ($customer && $customer->getId()) {
                $buyerId = $customer->getMstsImBuyerId();
                $customerStatus = $customer->getMstsImStatus();
            }
        }

        // If customer has no InvoiceMe buyer status assigned yet, they will be redirected to the InvoiceMe credit application form
        if (!$customerStatus) {
            $payment->setIsTransactionPending(true);
            return parent::authorize($payment, $amount);
        }

        $api = $helper->getApi();
        $api->setSellerId($helper->getSellerId())
            ->setBuyerId(Mage::getModel('core/encryption')->decrypt($buyerId))
            ->setCurrency($order->getBaseCurrencyCode())
            ->setPreauthorizedAmount($amount);

        try {
            $api->callCreateAPreauthorization();
        } catch (MSTS_InvoiceMe_ApiErrorResponseException $e) {
            $message = $e->getMessage();
            $httpStatusCode = $e->getCode();
            if (402 == $httpStatusCode) {
                $programUrl = $helper->getProgramUrl();
                $message = $helper->__('Hold on! You currently have insufficient credit, for this purchase. No worries, please visit %s to request an increase to your credit line.', $programUrl);
            } elseif (400 == $httpStatusCode) {
                $apiErrorCode = $e->getApiErrorCode();
                if ('preauthorization_po_required' === $apiErrorCode) {
                    $message = $helper->__('Purchase Order number is required');
                } elseif ('preauthorization_invalid_po' === $apiErrorCode) {
                    $message = $helper->__('Purchase Order number is invalid or does not match expected format');
                }
            }

            Mage::throwException($message);
        }

        if (!$api->getStatus()) {
            Mage::throwException($helper->__('Payment authorization error.'));
        }

        switch ($api->getStatus()) {
            case self::PREAUTHORIZATION_RESPONSE_STATUS_PREAUTHORIZED:
                $payment->setAdditionalInformation('payment_type', $this->getConfigData('payment_action'));
                $payment->setTransactionId($api->getId())->setIsTransactionClosed(0);
                $this->importToPaymentTransaction($api, $payment);
                return $this;
            default:
                Mage::throwException($helper->__('Payment authorization error. Received status %s', $api->getStatus()));
        }
    }

    /**
     * Send capture request to API
     *
     * @param  Varien_Object $payment
     * @param  float $amount
     * @return MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe
     * @throws Mage_Core_Exception
     * @throws Exception
     */
    public function capture(Varien_Object $payment, $amount)
    {
        /** @var Mage_Sales_Model_Order_Payment $payment */
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        if ($amount <= 0) {
            Mage::throwException($helper->__('Invalid amount for capture.'));
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $payment->getOrder();
        if ($order->getCustomerIsGuest()) {
            Mage::throwException($helper->__('InvoiceMe payment method is available to registered customers only.'));
        }

        if ($amount > $payment->getBaseAmountAuthorized()) {
            Mage::throwException(
                $helper->__('Invalid amount for capture. Captured amount can\'t be higher than the authorized amount.')
            );
        }

        $buyerId = $buyerStatus = null;
        $this->getBuyerIdAndStatusFromOrder($order, $buyerId, $buyerStatus);

        // If customer has no InvoiceMe buyer status assigned yet,
        // they will be redirected to the InvoiceMe credit application form
        if (!$buyerStatus) {
            $payment->setIsTransactionPending(true);
            return parent::capture($payment, $amount);
        }

        /** @var Mage_Sales_Model_Order_Invoice $currentInvoice */
        $currentInvoice = Mage::registry('current_invoice');
        if ($currentInvoice) {
            $details = $this->prepareCaptureDetailsFromInvoice($currentInvoice);
            $shippingAmount = floatval($currentInvoice->getBaseShippingAmount());
            $shippingTaxAmount = floatval($currentInvoice->getBaseShippingTaxAmount())
                + floatval($currentInvoice->getBaseShippingHiddenTaxAmount());
            $taxAmount = floatval($currentInvoice->getBaseTaxAmount())
                + floatval($currentInvoice->getBaseHiddenTaxAmount()) - $shippingTaxAmount;
            $discountAmountIncludingShippingDiscount = abs(
                $this->removeMinorDecimalPlacesFromFloat($currentInvoice->getBaseDiscountAmount())
            );
            $detailsItemsDiscount = 0;
            foreach ($details as $detailsItem) {
                if (isset($detailsItem['discount_amount'])) {
                    $detailsItemsDiscount += $detailsItem['discount_amount'];
                }
            }

            $shippingDiscountAmount = $discountAmountIncludingShippingDiscount - $detailsItemsDiscount;
            $shippingDiscountAmount = $this->removeMinorDecimalPlacesFromFloat($shippingDiscountAmount);
            $discountAmount = $this->removeMinorDecimalPlacesFromFloat($detailsItemsDiscount);
        } else {
            $details = $this->prepareCaptureDetailsFromOrder($order);
            $shippingAmount = floatval($payment->getBaseShippingAmount());
            $shippingTaxAmount = floatval($order->getBaseShippingTaxAmount())
                + floatval($order->getBaseShippingHiddenTaxAmount());
            $shippingDiscountAmount = abs(floatval($order->getBaseShippingDiscountAmount()));
            $taxAmount = floatval($order->getBaseTaxAmount())
                + floatval($order->getBaseHiddenTaxAmount()) - $shippingTaxAmount;
            $discountAmountIncludingShippingDiscount = abs(
                $this->removeMinorDecimalPlacesFromFloat($order->getBaseDiscountAmount())
            );
            $discountAmount = $discountAmountIncludingShippingDiscount - $shippingDiscountAmount;
            $discountAmount = $this->removeMinorDecimalPlacesFromFloat($discountAmount);
        }

        $taxAmount = $this->removeMinorDecimalPlacesFromFloat($taxAmount);

        $api = $helper->getApi();
        $api->setSellerId($helper->getSellerId())
            ->setBuyerId(Mage::getModel('core/encryption')->decrypt($buyerId))
            ->setCurrency($order->getBaseCurrencyCode())
            ->setTotalAmount($amount)
            ->setTaxAmount($taxAmount)
            ->setDiscountAmount($discountAmount)
            ->setShippingAmount($shippingAmount)
            ->setShippingDiscountAmount($shippingDiscountAmount)
            ->setShippingTaxAmount($shippingTaxAmount)
            ->setOrderUrl(Mage::getUrl('sales/order/view', ['order_id' => $order->getId()]))
            ->setOrderNumber($order->getIncrementId())
            ->setDetails($details);

        $shipTo = $this->prepareShipToFromOrder($order);
        if (!empty($shipTo)) {
            $api->setShipTo($shipTo);
        }

        $poNumber = $payment->getAdditionalInformation(self::FORM_FIELD_PO_NUMBER);
        if ($poNumber) {
            $api->setPoNumber($poNumber);
        }

        $notes = $payment->getAdditionalInformation(self::FORM_FIELD_NOTES);
        if ($notes) {
            $api->setComment($notes);
        }

        if ($payment->getParentTransactionId()) {
            $api->setPreauthorizationId($payment->getParentTransactionId());
        } else if ($payment->getReferenceTransactionId()) {
            $api->setPreauthorizationId($payment->getReferenceTransactionId());
        }

        if ($txn = $payment->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE)
        ) {
            $api->setPreviousChargeId($txn->getTxnId());
        }

        try {
            $api->callCreateACharge();
        } catch (MSTS_InvoiceMe_ApiErrorResponseException $e) {
            $message = $e->getMessage();
            $httpStatusCode = $e->getCode();
            if (402 == $httpStatusCode) {
                $programUrl = $helper->getProgramUrl();
                $message = $helper->__('Hold on! You currently have insufficient credit, for this purchase. No worries, please visit %s to request an increase to your credit line.', $programUrl);
            } elseif (400 == $httpStatusCode) {
                $apiErrorCode = $e->getApiErrorCode();
                if ('po_required' === $apiErrorCode) {
                    $message = $helper->__('Purchase Order number is required');
                } elseif ('invalid_po' === $apiErrorCode) {
                    $message = $helper->__('Purchase Order number is invalid or does not match expected format');
                }
            }

            Mage::throwException($message);
        }

        if (!$api->getStatus()) {
            Mage::throwException($helper->__('Payment capturing error.'));
        }

        switch ($api->getStatus()) {
            case self::CHARGE_RESPONSE_STATUS_CREATED:
                $payment->setTransactionId($api->getId())->setIsTransactionClosed(0);
                $this->importToPaymentTransaction($api, $payment);
                return $this;
            default:
                Mage::throwException($helper->__('Payment capturing error.'));
        }
    }

    /**
     * Cancel the payment through API
     *
     * @param Varien_Object $payment
     * @return MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe
     * @throws Mage_Core_Exception
     * @throws Exception
     */
    public function cancel(Varien_Object $payment)
    {
        return $this->void($payment);
    }

    /**
     * Void the payment through API
     *
     * @param Varien_Object $payment
     * @return MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe
     * @throws Mage_Core_Exception
     * @throws Exception
     */
    public function void(Varien_Object $payment)
    {
        /** @var Mage_Sales_Model_Order_Payment $payment */
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        if ($authTransaction = $payment->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH)) {
            $preauthorizationStatus = $authTransaction->getAdditionalInformation('status');
            if (self::PREAUTHORIZATION_RESPONSE_STATUS_PREAUTHORIZED !== $preauthorizationStatus) {
                Mage::throwException(
                    $helper->__(
                        'Cannot void the preauthorization transaction because its status is %s.',
                        $preauthorizationStatus
                    )
                );
            }

            $api = $helper->getApi();
            $api->setPathIdParam($authTransaction->getTxnId());
            $api->callCancelAPreauthorization();

            if (!$api->getStatus()) {
                Mage::throwException($helper->__('Payment voiding error.'));
            }

            switch ($api->getStatus()) {
                case self::PREAUTHORIZATION_RESPONSE_STATUS_CANCELLED:
                    $payment->setIsTransactionClosed(1)
                        ->setShouldCloseParentTransaction(1);
                    $this->importToPaymentTransaction($api, $payment);

                    $authTransaction->setAdditionalInformation(
                        'status',
                        self::PREAUTHORIZATION_RESPONSE_STATUS_CANCELLED
                    );
                    return $this;
                default:
                    Mage::throwException($helper->__('Payment voiding error. Received status %s', $api->getStatus()));
            }
        } else {
            Mage::throwException($helper->__('Preauthorization transaction is required to void.'));
        }

        return $this;
    }

    /**
     * Refund capture through the API
     *
     * @param Varien_Object $payment
     * @param float $amount
     * @return MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe
     */
    public function refund(Varien_Object $payment, $amount)
    {
        /** @var Mage_Sales_Model_Order_Payment $payment */
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        if ($amount <= 0) {
            Mage::throwException($helper->__('Invalid amount for a refund.'));
        }

        if ($amount > $payment->getBaseAmountPaid()) {
            Mage::throwException(
                $helper->__('Invalid amount for a refund. Refund amount can\'t be higher than the paid amount.')
            );
        }

        /** @var Mage_Sales_Model_Order_Creditmemo $creditMemo */
        $creditMemo = Mage::registry('current_creditmemo');

        /** @var Mage_Sales_Model_Order $order */
        $order = $payment->getOrder();

        $reason = 'Other';

        $api = $helper->getApi();
        $api->setPathIdParam($payment->getParentTransactionId());

        /** @var Mage_Sales_Model_Order_Invoice $invoice */
        $invoice = $creditMemo->getInvoice();
        if (!$invoice || !$invoice->getId()) {
            Mage::throwException($helper->__('Invoice couldn\'t be loaded'));
        }

        $orderRefundedShippingAmount = $order->getOrigData('base_shipping_refunded') ? floatval($order->getOrigData('base_shipping_refunded')) : 0;
        $orderShippingAmountLeft = floatval($order->getBaseShippingAmount()) - $orderRefundedShippingAmount;
        $invoiceShippingAmount = floatval($invoice->getBaseShippingAmount());
        $creditMemoShippingAmount = floatval($creditMemo->getBaseShippingAmount());
        if ($creditMemoShippingAmount > $invoiceShippingAmount) {
            Mage::throwException(
                $helper->__(
                    "Refunded shipping '%s' is higher than invoiced shipping '%s'",
                    $creditMemoShippingAmount,
                    $invoiceShippingAmount
                )
            );
        }

        if ($creditMemoShippingAmount > $orderShippingAmountLeft) {
            Mage::throwException(
                $helper->__(
                    "Refunded shipping '%s' is higher than captured shipping '%s'",
                    $creditMemoShippingAmount,
                    $orderShippingAmountLeft
                )
            );
        }

        $isFullRefund = $this->isFullRefund($creditMemo, $invoice, $amount);
        $remainingInvoiceDetails = $this->prepareRemainingInvoiceDetails($invoice);
        $details = $this->prepareRefundDetailsFromCreditmemo($creditMemo, $remainingInvoiceDetails);

        $newTotalAmount = $remainingInvoiceDetails['base_grand_total'] - $amount;
        $newTotalAmount = $this->removeMinorDecimalPlacesFromFloat($newTotalAmount);
        if ($newTotalAmount <= 0) {
            $isFullRefund = true;
        }

        try {
            if ($isFullRefund) {
                $api->setReason($reason);
                $api->callCancelACharge();
            } else {
                $newShippingAmount = $remainingInvoiceDetails['base_shipping_amount'] - $creditMemoShippingAmount;
                $newShippingTaxAmount = $remainingInvoiceDetails['base_shipping_tax_amount']
                    + floatval($remainingInvoiceDetails['base_shipping_hidden_tax_amount'])
                    - $creditMemo->getBaseShippingTaxAmount()
                    - $creditMemo->getBaseShippingHiddenTaxAmount();
                $newShippingTaxAmount = $this->removeMinorDecimalPlacesFromFloat($newShippingTaxAmount);
                $newTaxAmount = floatval($remainingInvoiceDetails['base_tax_amount'])
                    + floatval($remainingInvoiceDetails['base_hidden_tax_amount'])
                    - $creditMemo->getBaseTaxAmount()
                    - $creditMemo->getBaseHiddenTaxAmount()
                    - $newShippingTaxAmount;
                $newDiscountAmountIncludingShippingDiscount = abs($remainingInvoiceDetails['base_discount_amount'])
                    - abs($creditMemo->getBaseDiscountAmount());

                $newShippingAmount = $this->removeMinorDecimalPlacesFromFloat($newShippingAmount);
                $newTaxAmount = $this->removeMinorDecimalPlacesFromFloat($newTaxAmount);
                $newDiscountAmountIncludingShippingDiscount = $this->removeMinorDecimalPlacesFromFloat(
                    $newDiscountAmountIncludingShippingDiscount
                );

                $detailsItemsDiscount = 0;
                foreach ($details as $detailsItem) {
                    if (isset($detailsItem['discount_amount'])) {
                        $detailsItemsDiscount += $detailsItem['discount_amount'];
                    }
                }

                $newShippingDiscountAmount = $newDiscountAmountIncludingShippingDiscount - $detailsItemsDiscount;
                $newShippingDiscountAmount = $this->removeMinorDecimalPlacesFromFloat($newShippingDiscountAmount);
                $newDiscountAmount = abs($this->removeMinorDecimalPlacesFromFloat($detailsItemsDiscount));

                $api->setReturnAmount($amount)
                    ->setTotalAmount($newTotalAmount)
                    ->setTaxAmount($newTaxAmount)
                    ->setShippingAmount($newShippingAmount)
                    ->setDiscountAmount($newDiscountAmount)
                    ->setShippingTaxAmount($newShippingTaxAmount)
                    ->setShippingDiscountAmount($newShippingDiscountAmount)
                    ->setReturnReason($reason)
                    ->setDetails($details);
                $api->callReturnACharge();
            }
        } catch (MSTS_InvoiceMe_ApiErrorResponseException $e) {
            $message = $e->getMessage();
            Mage::throwException($message);
        }

        if (!$api->getStatus()) {
            Mage::throwException($helper->__('Payment refund error.'));
        }

        if (($api->getStatus() == self::CHARGE_RESPONSE_STATUS_CREATED && !$isFullRefund)
            || ($api->getStatus() == self::CHARGE_RESPONSE_STATUS_CANCELLED && $isFullRefund)
        ) {
            $suffix = '-refund-' . time();
            $payment->setTransactionId($api->getId() . $suffix)->setIsTransactionClosed(1);
            $payment->setShouldCloseParentTransaction(!$payment->getCreditmemo()->getInvoice()->canRefund());
            $this->importToPaymentTransaction($api, $payment);
            return $this;
        } else {
            Mage::throwException($helper->__('Payment refund error.'));
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function assignData($data)
    {
        $result = parent::assignData($data);

        $formData = [self::FORM_FIELD_PO_NUMBER, self::FORM_FIELD_NOTES];
        if (is_array($data)) {
            foreach ($formData as $key) {
                $value = isset($data[$key]) ? $data[$key] : null;
                $this->validateFormFieldMaximumLength($key, $value);
                $this->getInfoInstance()->setAdditionalInformation($key, $value);
            }
        } elseif ($data instanceof Varien_Object) {
            foreach ($formData as $key) {
                $value = $data->getData($key);
                $this->validateFormFieldMaximumLength($key, $value);
                $this->getInfoInstance()->setAdditionalInformation($key, $value);
            }
        }

        return $result;
    }

    /**
     * @param string $fieldKey
     * @param string|null $value
     * @throws Mage_Core_Exception
     */
    protected function validateFormFieldMaximumLength($fieldKey, $value)
    {
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');
        $formData = [
            self::FORM_FIELD_PO_NUMBER  => [
                'maxlength' => self::FORM_FIELD_PO_NUMBER_MAXIMUM_LENGTH,
                'label'     => $helper->__('Purchase Order Number'),
            ],
            self::FORM_FIELD_NOTES      => [
                'maxlength' => self::FORM_FIELD_NOTES_MAXIMUM_LENGTH,
                'label'     => $helper->__('Notes'),
            ],
        ];
        if ($value) {
            if (strlen($value) > $formData[$fieldKey]['maxlength']) {
                throw new Mage_Core_Exception(
                    $helper->__(
                        '%s is too long. The maximum length is %s characters.',
                        $formData[$fieldKey]['label'],
                        $formData[$fieldKey]['maxlength']
                    )
                );
            }
        }
    }

    /**
     * @param float $value
     * @return float
     */
    protected function removeMinorDecimalPlacesFromFloat($value)
    {
        return round($value, 5);
    }

    /**
     * @param Mage_Sales_Model_Order_Creditmemo $creditMemo
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @param float $amount
     * @return bool
     */
    protected function isFullRefund($creditMemo, $invoice, $amount)
    {
        $invoiceItems = $invoice->getAllItems();
        $creditMemoItems = $creditMemo->getAllItems();
        if (count($invoiceItems) == count($creditMemoItems) && $amount == $invoice->getGrandTotal()) {
            return true;
        }

        return false;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param $buyerId
     * @param $buyerStatus
     *
     * @throws Mage_Core_Exception
     */
    protected function getBuyerIdAndStatusFromOrder(Mage_Sales_Model_Order $order, &$buyerId, &$buyerStatus)
    {
        $customer = $order->getCustomer();
        if ($customer && $customer->getId()) {
            $buyerId = $order->getCustomer()->getMstsImBuyerId();
            $buyerStatus = $order->getCustomer()->getMstsImStatus();
        } else if ($order->getCustomerId()) {
            $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
            if ($customer && $customer->getId()) {
                $buyerId = $customer->getMstsImBuyerId();
                $buyerStatus = $customer->getMstsImStatus();
            }
        }
    }

    /**
     * @param MSTS_InvoiceMe_Model_Api_InvoiceMe $api
     * @param Mage_Sales_Model_Order_Payment $payment
     */
    protected function importToPaymentTransaction(
        MSTS_InvoiceMe_Model_Api_InvoiceMe $api,
        Mage_Sales_Model_Order_Payment $payment
    ) {
        $transactionAdditionalInfoKeys = [
            'currency',
            'preauthorized_amount',
            'captured_amount',
            'total_amount',
            'tax_amount',
            'discount_amount',
            'shipping_amount',
            'shipping_tax_amount',
            'shipping_discount_amount',
            'foreign_exchange_fee',
            'original_total_amount',
            'paid_amount_currency',
            'paid_amount',
            'status',
            'po_number',
            'previous_charge_id',
            'comment',
            'ship_to',
            'due_date',
            'expires',
            'created',
            'modified',
            'details',
            'cancellation_reason',
            'return_reason',
        ];

        foreach ($transactionAdditionalInfoKeys as $key) {
            if ($api->hasData($key)) {
                $value = $api->getDataUsingMethod($key);
                if (is_array($value)) {
                    $value = Mage::helper('core')->jsonEncode($value);
                }

                $payment->setTransactionAdditionalInfo($key, $value);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function canUseForCurrency($currencyCode)
    {
        if (!in_array($currencyCode, $this->getAcceptedCurrencyCodes())) {
            return false;
        }

        return true;
    }

    /**
     * Return array of currency codes supplied by Payment Gateway
     *
     * @return array
     */
    public function getAcceptedCurrencyCodes()
    {
        if (!$this->hasData('_accepted_currency')) {
            $acceptedCurrencyCodes = $this->_allowCurrencyCode;
            $acceptedCurrencyCodes[] = $this->getConfigData('currency');
            $this->setData('_accepted_currency', $acceptedCurrencyCodes);
        }

        return $this->_getData('_accepted_currency');
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function prepareCaptureDetailsFromOrder($order)
    {
        $items = [];
        foreach ($order->getItemsCollection() as $item) {
            /** @var Mage_Sales_Model_Order_Item $item */
            $qty = $item->getQtyOrdered();
            $isConfigurable = (Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE == $item->getProductType());
            $isBundle = (Mage_Catalog_Model_Product_Type::TYPE_BUNDLE == $item->getProductType());
            $isPriceFixedType = false;
            if ($isBundle) {
                $isPriceFixedType = ($item->getProduct()->getPriceType() == Mage_Bundle_Model_Product_Price::PRICE_TYPE_FIXED);
            }

            if ($qty > 0
                && $item->getBaseRowTotalInclTax() > 0
                && ($isConfigurable
                    || ($isBundle && $isPriceFixedType)
                    || !$item->getHasChildren()
                )
            ) {
                $discountAmount = $item->getBaseDiscountAmount() ?: 0;
                $items[] = [
                    'sku'               => $item->getSku(),
                    'description'       => $item->getName(),
                    'quantity'          => (int)$qty,
                    'unit_price'        => $item->getBasePrice(),
                    'tax_amount'        => $item->getBaseTaxAmount() + $item->getBaseHiddenTaxAmount(),
                    'discount_amount'   => floatval($discountAmount),
                    'subtotal'          => $this->calculateItemSubtotal($item),
                ];

                $this->addGiftWrappingForItemToDetailsItems(
                    floatval($item->getGwBasePriceInvoiced()),
                    $item->getGwBaseTaxAmountInvoiced(),
                    $item,
                    $qty,
                    $items
                );
            }
        }

        $this->addGiftWrappingForOrderToDetailsItems(
            floatval($order->getGwBasePriceInvoiced()),
            $order->getGwBaseTaxAmountInvoiced(),
            $items
        );

        $this->addPrintedCardToDetailsItems(
            floatval($order->getGwPrintedCardBasePriceInvoiced()),
            $order->getGwPrintedCardBaseTaxAmountInvoiced(),
            $items
        );

        $this->addGiftCardsToDetailsItems(
            $order->getBaseGiftCardsAmount(),
            $items,
            true
        );

        $this->addStoreCreditToDetailsItems(
            $order->getBaseCustomerBalanceAmount(),
            $items,
            true
        );

        return $items;
    }

    /**
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return array
     */
    protected function prepareCaptureDetailsFromInvoice($invoice)
    {
        $items = [];
        foreach ($invoice->getItemsCollection() as $item) {
            /** @var Mage_Sales_Model_Order_Invoice_Item $item */
            $qty = $item->getQty();
            if ($qty > 0 && $item->getBaseRowTotalInclTax() > 0) {
                $discountAmount = $item->getBaseDiscountAmount() ?: 0;
                $items[] = [
                    'sku'               => $item->getSku(),
                    'description'       => $item->getName(),
                    'quantity'          => (int)$qty,
                    'unit_price'        => floatval($item->getBasePrice()),
                    'tax_amount'        => $item->getBaseTaxAmount() + $item->getBaseHiddenTaxAmount(),
                    'discount_amount'   => floatval($discountAmount),
                    'subtotal'          => $this->calculateItemSubtotal($item),
                ];

                $orderItem = $item->getOrderItem();
                $this->addGiftWrappingForItemToDetailsItems(
                    floatval($orderItem->getGwBasePriceInvoiced()),
                    floatval($orderItem->getGwBaseTaxAmountInvoiced()),
                    $orderItem,
                    floatval($qty),
                    $items
                );
            }
        }

        $this->addGiftWrappingForOrderToDetailsItems(
            floatval($invoice->getGwBasePrice()),
            floatval($invoice->getGwBaseTaxAmount()),
            $items
        );

        $this->addPrintedCardToDetailsItems(
            floatval($invoice->getGwPrintedCardBasePrice()),
            floatval($invoice->getGwPrintedCardBaseTaxAmount()),
            $items
        );

        $this->addGiftCardsToDetailsItems(
            $invoice->getBaseGiftCardsAmount(),
            $items,
            true
        );

        $this->addStoreCreditToDetailsItems(
            $invoice->getBaseCustomerBalanceAmount(),
            $items,
            true
        );

        return $items;
    }

    /**
     * @param Mage_Sales_Model_Order_Invoice_Item $item
     * @return float
     */
    protected function calculateItemSubtotal($item)
    {
        $discountAmount = $item->getBaseDiscountAmount() ?: 0;
        $subtotal = $item->getBaseRowTotal() + floatval($item->getBaseTaxAmount()) - floatval($discountAmount);
        $this->includeHiddenTaxInItemSubtotal($subtotal, $item);

        return $this->removeMinorDecimalPlacesFromFloat($subtotal);
    }

    /**
     * @param float $subtotal
     * @param Mage_Sales_Model_Order_Invoice_Item $item
     */
    protected function includeHiddenTaxInItemSubtotal(&$subtotal, $item)
    {
        /** @var Mage_Tax_Helper_Data $taxHelper */
        $taxHelper = Mage::helper('tax');
        if ($taxHelper->priceIncludesTax()) {
            $calculationSequence = $taxHelper->getCalculationSequence();
            $hiddenTaxSequences = [
                Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_EXCL,
                Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_INCL,
            ];
            if (in_array($calculationSequence, $hiddenTaxSequences)) {
                $hiddenTaxAmount = $item->getBaseHiddenTaxAmount();
                $subtotal += floatval($hiddenTaxAmount);
            }
        }
    }

    /**
     * @param float $gwPrice
     * @param float $gwTaxAmount
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @param int $qty
     * @param array $items
     * @param bool $shouldSubtract
     */
    protected function addGiftWrappingForItemToDetailsItems(
        $gwPrice,
        $gwTaxAmount,
        $orderItem,
        $qty,
        &$items,
        $shouldSubtract = false
    ) {
        if ($gwPrice > 0) {
            $this->addItemToDetailsItems(
                'gw_' . $orderItem->getSku(),
                'Gift Wrapping for ' . $orderItem->getName(),
                $gwPrice,
                $qty,
                $gwTaxAmount,
                $items,
                $shouldSubtract
            );
        }
    }

    /**
     * @param float $gwPrice
     * @param float $gwTaxAmount
     * @param array $items
     * @param bool $shouldSubtract
     */
    protected function addGiftWrappingForOrderToDetailsItems($gwPrice, $gwTaxAmount, &$items, $shouldSubtract = false)
    {
        if ($gwPrice > 0) {
            $this->addItemToDetailsItems(
                'gw_order',
                'Gift Wrapping for order',
                $gwPrice,
                1,
                $gwTaxAmount,
                $items,
                $shouldSubtract
            );
        }
    }

    /**
     * @param float $printedCardPrice
     * @param float $printedCardTaxAmount
     * @param array $items
     * @param bool $shouldSubtract
     */
    protected function addPrintedCardToDetailsItems(
        $printedCardPrice,
        $printedCardTaxAmount,
        &$items,
        $shouldSubtract = false
    ) {
        if ($printedCardPrice > 0) {
            $this->addItemToDetailsItems(
                'gw_printed_card',
                'Printed Card',
                $printedCardPrice,
                1,
                $printedCardTaxAmount,
                $items,
                $shouldSubtract
            );
        }
    }

    /**
     * @param float $giftCardsAmount
     * @param array $items
     * @param bool $shouldSubtract
     */
    protected function addGiftCardsToDetailsItems($giftCardsAmount, &$items, $shouldSubtract = false)
    {
        if ($giftCardsAmount > 0) {
            $this->addItemToDetailsItems(
                'gift_cards',
                'Gift Cards',
                $giftCardsAmount,
                1,
                0,
                $items,
                $shouldSubtract
            );
        }
    }

    /**
     * @param float $customerBalanceAmount
     * @param array $items
     * @param bool $shouldSubtract
     */
    protected function addStoreCreditToDetailsItems($customerBalanceAmount, &$items, $shouldSubtract = false)
    {
        if ($customerBalanceAmount > 0) {
            $this->addItemToDetailsItems(
                'store_credit',
                'Store Credit',
                $customerBalanceAmount,
                1,
                0,
                $items,
                $shouldSubtract
            );
        }
    }

    /**
     * @param string $sku
     * @param string $description
     * @param float $unitPrice
     * @param int $qty
     * @param float $taxAmount
     * @param array $items
     * @param bool $shouldSubtract
     */
    protected function addItemToDetailsItems($sku, $description, $unitPrice, $qty, $taxAmount, &$items, $shouldSubtract)
    {
        $subtotal = ($qty * $unitPrice) + ($qty * $taxAmount);
        $items[] = [
            'sku'           => $sku,
            'description'   => $description,
            'quantity'      => $qty,
            'unit_price'    => $shouldSubtract ? -$unitPrice : $unitPrice,
            'tax_amount'    => $shouldSubtract ? -($qty * $taxAmount) : ($qty * $taxAmount),
            'subtotal'      => $shouldSubtract ? -$subtotal : $subtotal,
        ];
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     * @throws Exception
     */
    protected function prepareShipToFromOrder(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if (!$shippingAddress) {
            $shippingAddress = $order->getBillingAddress();
        }

        $shipTo = [];
        $shipTo['recipient_name'] = substr(
            $shippingAddress->getName(),
            0,
            self::SHIP_TO_RECIPIENT_NAME_MAXIMUM_LENGTH
        );
        if ($shippingAddress->getCompany()) {
            $shipTo['company_name'] = substr(
                $shippingAddress->getCompany(),
                0,
                self::SHIP_TO_COMPANY_NAME_MAXIMUM_LENGTH
            );
        }
        $shipTo['company_address'] = $this->prepareCompanyAddress($shippingAddress);
        $tracking = $this->prepareTracking($order);
        if (!empty($tracking)) {
            $shipTo['tracking'] = $tracking;
        }

        return $shipTo;
    }

    /**
     * @param Mage_Sales_Model_Order_Address $shippingAddress
     * @return array
     */
    protected function prepareCompanyAddress(Mage_Sales_Model_Order_Address $shippingAddress)
    {
        if (!$shippingAddress) {
            return [];
        }

        $companyAddress = [];
        $companyAddress['address_line1'] = substr(
            $shippingAddress->getStreet1(),
            0,
            self::SHIP_TO_ADDRESS_LINE_MAXIMUM_LENGTH
        );
        $street2 = $shippingAddress->getStreet2();
        if ($street2) {
            $companyAddress['address_line2'] = substr(
                $street2,
                0,
                self::SHIP_TO_ADDRESS_LINE_MAXIMUM_LENGTH
            );
        }
        $companyAddress['country'] = $shippingAddress->getCountry();
        $companyAddress['city'] = substr(
            $shippingAddress->getCity(),
            0,
            self::SHIP_TO_CITY_MAXIMUM_LENGTH
        );
        $state = $shippingAddress->getRegionCode();
        if (!$state) {
            $state = $shippingAddress->getRegion();
        }
        if ($state) {
            $companyAddress['state'] = substr(
                $state,
                0,
                self::SHIP_TO_STATE_MAXIMUM_LENGTH
            );
        }
        $companyAddress['zip'] = substr(
            $shippingAddress->getPostcode(),
            0,
            self::SHIP_TO_ZIP_MAXIMUM_LENGTH
        );

        return $companyAddress;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     * @throws Exception
     */
    protected function prepareTracking(Mage_Sales_Model_Order $order)
    {
        $trackData = $this->getTrackData($order);
        if (!$trackData) {
            return null;
        }

        $trackingData = [];
        $trackingData['tracking_number'] = substr(
            $trackData['number'],
            0,
            self::SHIP_TO_TRACKING_NUMBER_MAXIMUM_LENGTH
        );
        $trackingUrl = $trackData['url'];
        if ($trackingUrl) {
            $trackingData['tracking_url'] = substr(
                $trackingUrl,
                0,
                self::SHIP_TO_TRACKING_URL_MAXIMUM_LENGTH
            );
        }
        $carrier = $trackData['carrier'];
        if ($carrier) {
            $trackingData['carrier'] = substr(
                $carrier,
                0,
                self::SHIP_TO_CARRIER_MAXIMUM_LENGTH
            );
        }

        return $trackingData;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     * @throws Exception
     */
    protected function getTrackData(Mage_Sales_Model_Order $order)
    {
        $tracksCollection = $order->getTracksCollection();
        if ($tracksCollection->getSize()) {
            /** @var Mage_Sales_Model_Order_Shipment_Track $track */
            $track = $tracksCollection->getFirstItem();

            return [
                'number' => $track->getNumber(),
                'url' => $track->getUrl(),
                'carrier' => $track->getTitle(),
            ];
        }

        $data = $this->getRequest()->getPost('invoice');
        if (!empty($data['do_shipment'])) {
            $tracks = $this->getRequest()->getPost('tracking');

            if ($tracks) {
                foreach ($tracks as $trackData) {
                    /** @var Mage_Sales_Model_Order_Shipment_Track $track */
                    $track = Mage::getModel('sales/order_shipment_track')
                        ->addData($trackData);

                    return [
                        'number' => $track->getNumber(),
                        'url' => $track->getUrl(),
                        'carrier' => $track->getTitle(),
                    ];
                }
            }
        }

        return null;
    }

    /**
     * Retrieve request object
     *
     * @return Mage_Core_Controller_Request_Http
     * @throws Exception
     */
    protected function getRequest()
    {
        $controller = Mage::app()->getFrontController();
        if ($controller) {
            $this->request = $controller->getRequest();
        } else {
            throw new Exception(Mage::helper('core')->__("Can't retrieve request object"));
        }

        return $this->request;
    }

    /**
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @param array $remainingInvoiceDetails
     * @return array
     */
    protected function prepareRefundDetailsFromCreditmemo(
        Mage_Sales_Model_Order_Creditmemo $creditmemo,
        array $remainingInvoiceDetails
    ) {
        $items = [];
        $this->addRemainingInvoiceItemsToRefundDetails($remainingInvoiceDetails, $items, $creditmemo);
        $this->addCreditmemoItemsToRefundDetails($creditmemo, $items);
        $this->addRefundAdjustmentItemsToRefundDetails($creditmemo, $items);
        return $items;
    }

    /**
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @param array $invoiceItems
     * @return $this
     */
    protected function subtractCreditMemoQtyFromInvoiceItems(
        Mage_Sales_Model_Order_Creditmemo $creditmemo,
        array &$invoiceItems
    ) {
        $creditmemoItems = $creditmemo->getAllItems();
        foreach ($invoiceItems as $invoiceItem) {
            /** @var Mage_Sales_Model_Order_Invoice_Item $invoiceItem */
            $qty = $invoiceItem->getQty();
            $taxAmount = $invoiceItem->getBaseTaxAmount();
            $hiddenTaxAmount = $invoiceItem->getBaseHiddenTaxAmount();
            $discountAmount = $invoiceItem->getBaseDiscountAmount();
            $rowTotal = $invoiceItem->getBaseRowTotal();
            $rowTotalInclTax = $invoiceItem->getBaseRowTotalInclTax();
            foreach ($creditmemoItems as $creditmemoItem) {
                /** @var Mage_Sales_Model_Order_Creditmemo_Item $creditmemoItem */
                if ($invoiceItem->getOrderItemId() == $creditmemoItem->getOrderItemId()) {
                    $qty -= $creditmemoItem->getQty();
                    $taxAmount -= $creditmemoItem->getBaseTaxAmount();
                    $hiddenTaxAmount -= $creditmemoItem->getBaseHiddenTaxAmount();
                    $discountAmount -= $creditmemoItem->getBaseDiscountAmount();
                    $rowTotal -= $creditmemoItem->getBaseRowTotal();
                    $rowTotalInclTax -= $creditmemoItem->getBaseRowTotalInclTax();
                    break;
                }
            }

            $invoiceItem->setQty($qty);
            $invoiceItem->setBaseTaxAmount($taxAmount);
            $invoiceItem->setBaseHiddenTaxAmount($hiddenTaxAmount);
            $invoiceItem->setBaseDiscountAmount($discountAmount);
            $invoiceItem->setBaseRowTotal($rowTotal);
            $invoiceItem->setBaseRowTotalInclTax($rowTotalInclTax);
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return array
     */
    protected function prepareRemainingInvoiceDetails(Mage_Sales_Model_Order_Invoice $invoice) 
    {
        $invoiceFieldsToCopy = [
            'base_grand_total',
            'base_tax_amount',
            'base_hidden_tax_amount',
            'base_discount_amount',
            'base_shipping_amount',
            'base_shipping_tax_amount',
            'base_shipping_hidden_tax_amount',
            'base_subtotal',
            'order_id',
            'increment_id',
        ];
        $invoiceDetails  = [];
        foreach ($invoiceFieldsToCopy as $field) {
            $invoiceDetails[$field] = $invoice->getDataUsingMethod($field);
        }

        $invoiceDetails['items'] = [];
        foreach ($invoice->getAllItems() as $item) {
            $invoiceDetails['items'][] = $this->convertInvoiceItemToSimpleObject($item);
        }

        $this->subtractPreviousCreditMemosFromInvoiceDetails($invoice, $invoiceDetails);

        return $invoiceDetails;
    }

    /**
     * @param Mage_Sales_Model_Order_Invoice_Item $invoiceItem
     * @return Varien_Object
     */
    protected function convertInvoiceItemToSimpleObject(Mage_Sales_Model_Order_Invoice_Item $invoiceItem) 
    {
        $fieldsToCopy = [
            'qty',
            'sku',
            'name',
            'base_tax_amount',
            'base_hidden_tax_amount',
            'base_price',
            'base_discount_amount',
            'base_row_total',
            'base_row_total_incl_tax',
            'order_item_id',
        ];

        $simpleObject = new Varien_Object();
        foreach ($fieldsToCopy as $fieldName) {
            $simpleObject->setData($fieldName, $invoiceItem->getDataUsingMethod($fieldName));
        }

        return $simpleObject;
    }

    /**
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @param array $invoiceDetails
     * @return $this
     */
    protected function subtractPreviousCreditMemosFromInvoiceDetails(
        Mage_Sales_Model_Order_Invoice $invoice,
        array &$invoiceDetails
    ) {
        $invoiceCreditMemos = $this->getInvoiceCreditMemos($invoice);
        if ($invoiceCreditMemos && $invoiceCreditMemos->count()) {
            foreach ($invoiceCreditMemos as $previousCreditmemo) {
                /** @var Mage_Sales_Model_Order_Creditmemo $previousCreditmemo */
                $invoiceDetails['base_grand_total'] -= $previousCreditmemo->getBaseGrandTotal();
                $invoiceDetails['base_subtotal'] -= $previousCreditmemo->getBaseSubtotal();
                $invoiceDetails['base_shipping_amount'] -= $previousCreditmemo->getBaseShippingAmount();
                $invoiceDetails['base_shipping_tax_amount'] -= $previousCreditmemo->getBaseShippingTaxAmount();
                $invoiceDetails['base_shipping_hidden_tax_amount'] -= $previousCreditmemo->getBaseShippingHiddenTaxAmount();
                $invoiceDetails['base_tax_amount'] -= $previousCreditmemo->getBaseTaxAmount();
                $invoiceDetails['base_hidden_tax_amount'] -= $previousCreditmemo->getBaseHiddenTaxAmount();
                $invoiceDetails['base_discount_amount'] -= $previousCreditmemo->getBaseDiscountAmount();
                $this->subtractCreditMemoQtyFromInvoiceItems($previousCreditmemo, $invoiceDetails['items']);
            }
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return Mage_Sales_Model_Resource_Order_Creditmemo_Collection
     */
    protected function getInvoiceCreditMemos(Mage_Sales_Model_Order_Invoice $invoice)
    {
        /** @var Mage_Sales_Model_Resource_Order_Creditmemo_Collection $creditmemos */
        $creditmemos = Mage::getModel('sales/order_creditmemo')->getCollection();
        $creditmemos->addFieldToFilter('invoice_id', $invoice->getId());
        return $creditmemos;
    }

    /**
     * @param array $remainingInvoiceDetails
     * @param array $items
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @return $this
     */
    protected function addRemainingInvoiceItemsToRefundDetails($remainingInvoiceDetails, array &$items, $creditmemo)
    {
        if (isset($remainingInvoiceDetails['items'])) {
            foreach ($remainingInvoiceDetails['items'] as $item) {
                $qty = $item->getQty();
                if ($qty > 0 && $item->getBaseRowTotalInclTax() > 0) {
                    $discountAmount = $item->getBaseDiscountAmount() ?: 0;
                    $items[] = [
                        'sku'               => $item->getSku(),
                        'description'       => $item->getName(),
                        'quantity'          => floatval($qty),
                        'unit_price'        => floatval($item->getBasePrice()),
                        'tax_amount'        => floatval($item->getBaseTaxAmount())
                            + floatval($item->getBaseHiddenTaxAmount()),
                        'discount_amount'   => floatval($discountAmount),
                        'subtotal'          => $this->calculateItemSubtotal($item),
                    ];

                    $orderItem = $creditmemo->getOrder()->getItemById($item->getOrderItemId());
                    $this->addGiftWrappingForItemToDetailsItems(
                        floatval($orderItem->getGwBasePriceInvoiced()),
                        floatval($orderItem->getGwBaseTaxAmountInvoiced()),
                        $orderItem,
                        floatval($qty),
                        $items
                    );
                }
            }
        }

        $this->addGiftWrappingForOrderToDetailsItems(
            floatval($creditmemo->getGwBasePrice()),
            floatval($creditmemo->getGwBaseTaxAmount()),
            $items
        );

        $gwPrintedCardBaseTaxAmount = $creditmemo->getGwPrintedCardBaseTaxAmount() ? $creditmemo->getGwPrintedCardBaseTaxAmount() : 0;
        $this->addPrintedCardToDetailsItems(
            floatval($creditmemo->getGwPrintedCardBasePrice()),
            floatval($gwPrintedCardBaseTaxAmount),
            $items
        );

        $this->addGiftCardsToDetailsItems(
            $creditmemo->getBaseGiftCardsAmount(),
            $items,
            true
        );

        $this->addStoreCreditToDetailsItems(
            $creditmemo->getBaseCustomerBalanceAmount(),
            $items,
            true
        );

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @param array $items
     * @return $this
     */
    protected function addCreditmemoItemsToRefundDetails($creditmemo, array &$items)
    {
        foreach ($creditmemo->getAllItems() as $item) {
            $qty = $item->getQty();
            if ($qty > 0 && $item->getBaseRowTotalInclTax() > 0) {
                $discountAmount = $item->getBaseDiscountAmount() ?: 0;
                $items[] = [
                    'sku'               => $item->getSku(),
                    'description'       => $item->getName(),
                    'quantity'          => floatval($qty),
                    'unit_price'        => -floatval($item->getBasePrice()),
                    'tax_amount'        => -($item->getBaseTaxAmount() + $item->getBaseHiddenTaxAmount()),
                    'discount_amount'   => -floatval($discountAmount),
                    'subtotal'          => -$this->calculateItemSubtotal($item),
                ];

                $orderItem = $creditmemo->getOrder()->getItemById($item->getOrderItemId());
                $this->addGiftWrappingForItemToDetailsItems(
                    floatval($orderItem->getGwBasePriceInvoiced()),
                    floatval($orderItem->getGwBaseTaxAmountInvoiced()),
                    $orderItem,
                    $qty,
                    $items,
                    true
                );
            }
        }

        $this->addGiftWrappingForOrderToDetailsItems(
            floatval($creditmemo->getGwBasePrice()),
            floatval($creditmemo->getGwBaseTaxAmount()),
            $items,
            true
        );

        $gwPrintedCardBaseTaxAmount = $creditmemo->getGwPrintedCardBaseTaxAmount() ? $creditmemo->getGwPrintedCardBaseTaxAmount() : 0;
        $this->addPrintedCardToDetailsItems(
            floatval($creditmemo->getGwPrintedCardBasePrice()),
            floatval($gwPrintedCardBaseTaxAmount),
            $items,
            true
        );

        $this->addGiftCardsToDetailsItems(
            $creditmemo->getBaseGiftCardsAmount(),
            $items
        );

        $this->addStoreCreditToDetailsItems(
            $creditmemo->getBaseCustomerBalanceAmount(),
            $items
        );

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @param array $items
     * @return $this
     */
    protected function addRefundAdjustmentItemsToRefundDetails(
        Mage_Sales_Model_Order_Creditmemo $creditmemo,
        array &$items
    ) {
        $adjustmentNegative = $creditmemo->getAdjustmentNegative();
        if ($adjustmentNegative) {
            $items[] = [
                'sku'           => 'invoiceme-adjustment-fee',
                'description'   => Mage::helper('sales')->__('Adjustment Fee'),
                'quantity'      => 1,
                'unit_price'    => floatval($adjustmentNegative),
                'tax_amount'    => 0,
                'subtotal'      => floatval($adjustmentNegative),
            ];
        }

        $adjustmentPositive = $creditmemo->getAdjustmentPositive();
        if ($adjustmentPositive) {
            $items[] = [
                'sku'           => 'invoiceme-adjustment-refund',
                'description'   => Mage::helper('sales')->__('Adjustment Refund'),
                'quantity'      => 1,
                'unit_price'    => -floatval($adjustmentPositive),
                'tax_amount'    => 0,
                'subtotal'      => -floatval($adjustmentPositive),
            ];
        }

        return $this;
    }

    /**
     * Disallow partial invoice if Gift Cards or Store Credits used
     *
     * @inheritDoc
     */
    public function canCapturePartial()
    {
        $invoice = Mage::registry('current_invoice');
        if ($invoice) {
            if ($invoice->getOrder()->getBaseGiftCardsAmount() > 0) {
                return false;
            }

            if ($invoice->getOrder()->getBaseCustomerBalanceAmount() > 0) {
                return false;
            }
        }

        return $this->_canCapturePartial;
    }
}
