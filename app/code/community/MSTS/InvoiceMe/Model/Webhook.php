<?php

class MSTS_InvoiceMe_Model_Webhook
{
    const WEBHOOK_TYPE_BUYER_STATUS             = 'buyer.status';
    const WEBHOOK_TYPE_PREAUTHORIZATION_UPDATED = 'preauthorization.updated';

    /** @var MSTS_InvoiceMe_Helper_Data */
    protected $helper;
    /** @var string */
    protected $scope = 'default';
    /** @var null|string */
    protected $scopeCode = null;


    public function __construct()
    {
        $this->helper = Mage::helper('msts_invoiceme');
    }

    /**
     * @param string $scope
     * @return MSTS_InvoiceMe_Model_Webhook
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
        $this->helper->setScope($scope);
        return $this;
    }

    /**
     * @param null|string $scopeCode
     * @return MSTS_InvoiceMe_Model_Webhook
     */
    public function setScopeCode($scopeCode)
    {
        $this->scopeCode = $scopeCode;
        $this->helper->setScopeCode($scopeCode);
        return $this;
    }

    /**
     * @return array
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function listWebhooks()
    {
        $api = $this->getApi();
        $api->callListWebhooks();

        return $api->getResponse();
    }

    /**
     * @param string $webhookUrl
     * @param array $eventTypes
     * @param string $webhookAuthToken
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws Exception
     */
    public function createWebhook($webhookUrl, $eventTypes, $webhookAuthToken)
    {
        $api = $this->getApi();
        $api->setWebhookUrl($webhookUrl);
        $api->setWebhookAuthToken($webhookAuthToken);
        $api->setWebhookAuthTokenHeader('Authorization');
        $api->setEventTypes($eventTypes);
        $api->callCreateAWebhook();

        return $api;
    }

    /**
     * @param string $id
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function deleteWebhook($id = '')
    {
        $api = $this->getApi();
        $api->setApiKey($this->getApiKeyForCreatedWebhooks());
        $api->setPathIdParam($id);
        $api->callDeleteAWebhook();

        return $api;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     */
    protected function getApi()
    {
        return $this->helper->getApi();
    }

    /**
     * @return mixed
     */
    protected function getApiKeyForCreatedWebhooks()
    {
        return $this->helper->getApiKeyForCreatedWebhooks();
    }

    /**
     * @param mixed $webhooksData
     * @return bool
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function deleteWebhooks($webhooksData)
    {
        if (!is_array($webhooksData)) {
            return true;
        }

        foreach ($webhooksData as $index => $webhookData) {
            if (is_array($webhookData)) {
                if (isset($webhookData['id'])) {
                    $this->deleteWebhook($webhookData['id']);
                }
            } else {
                if ('id' == $index) {
                    $this->deleteWebhook($webhookData);
                    break;
                }
            }
        }

        return true;
    }

    /**
     * @return bool
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function deleteWebhooksIfApiKeyChanged()
    {
        if (!$this->helper->validateApiKeyForCreatedWebhooks()) {
            return $this->deleteWebhooks(Mage::helper('core')->jsonDecode($this->helper->getCreatedWebhooks()));
        }

        return true;
    }

    /**
     * @param string $baseSecureUrl
     * @return array
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function createWebhooks($baseSecureUrl)
    {
        $webhookAuthToken = $this->generateWebhookAuthToken();
        $this->createWebhook(
            $this->getWebhookUrl($baseSecureUrl, 'buyerStatus'),
            [self::WEBHOOK_TYPE_BUYER_STATUS],
            $webhookAuthToken
        );
        $this->createWebhook(
            $this->getWebhookUrl($baseSecureUrl, 'preauthorizationUpdated'),
            [self::WEBHOOK_TYPE_PREAUTHORIZATION_UPDATED],
            $webhookAuthToken
        );

        return $this->listWebhooks();
    }

    /**
     * @param array $webhooksData
     * @return bool
     */
    public function validateAllWebhooksCreated($webhooksData)
    {
        if (!is_array($webhooksData)) {
            return false;
        }

        $correctWebhooks = [
            MSTS_InvoiceMe_Model_Webhook::WEBHOOK_TYPE_BUYER_STATUS,
            MSTS_InvoiceMe_Model_Webhook::WEBHOOK_TYPE_PREAUTHORIZATION_UPDATED,
        ];
        $createdWebhooks = [];
        foreach ($webhooksData as $index => $webhookData) {
            if (is_array($webhookData)) {
                if (isset($webhookData['event_types'])) {
                    foreach ($webhookData['event_types'] as $eventType) {
                        $createdWebhooks[] = $eventType;
                    }
                }
            } else {
                return false;
            }
        }

        return !array_diff($correctWebhooks, $createdWebhooks);
    }

    /**
     * @param array $webhooksData
     */
    public function maskWebhooksCreatedValues(&$webhooksData)
    {
        foreach ($webhooksData as $index => $webhookData) {
            if (is_array($webhookData)) {
                if (isset($webhookData['webhook_auth_token'])) {
                    $tokenWithBearerStripped = str_replace('Bearer ', '', $webhookData['webhook_auth_token']);
                    $webhooksData[$index]['webhook_auth_token'] = 'Bearer ' . $this->helper->maskValue($tokenWithBearerStripped);
                }
            } else {
                if ('webhook_auth_token' == $index) {
                    $tokenWithBearerStripped = str_replace('Bearer ', '', $webhooksData['webhook_auth_token']);
                    $webhooksData['webhook_auth_token'] = 'Bearer ' . $this->helper->maskValue($tokenWithBearerStripped);
                }
            }
        }
    }

    /**
     * @return string
     */
    protected function generateWebhookAuthToken()
    {
        return 'Bearer ' . $this->helper->generateUuidV4();
    }

    /**
     * @param string $baseSecureUrl
     * @param string $eventTypeAction
     * @return string
     */
    protected function getWebhookUrl($baseSecureUrl, $eventTypeAction)
    {
        return $baseSecureUrl . 'msts_invoiceme/webhook/' . $eventTypeAction;
    }
}
