<?php

class MSTS_InvoiceMe_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     * @return MSTS_InvoiceMe_Model_Observer
     * @throws MSTS_InvoiceMe_NotUniqueClientReferenceIdException
     */
    public function validateClientReferenceIdUniqueness($observer)
    {
        $customer = $observer->getCustomer();
        if (!$customer->getMstsImClientReferenceId()) {
            return $this;
        }

        $customer = $observer->getCustomer();
        $adapter = Mage::getSingleton('core/resource')->getConnection('customer_read');
        $bind    = ['msts_im_client_reference_id' => $customer->getMstsImClientReferenceId()];
        $select = $adapter->select()
            ->from($customer->getResource()->getEntityTable(), [$customer->getResource()->getEntityIdField()])
            ->where('msts_im_client_reference_id = :msts_im_client_reference_id');
        if ($customer->getSharingConfig()->isWebsiteScope()) {
            $bind['website_id'] = (int)$customer->getWebsiteId();
            $select->where('website_id = :website_id');
        }

        if ($customer->getId()) {
            $bind['entity_id'] = (int)$customer->getId();
            $select->where('entity_id != :entity_id');
        }

        $result = $adapter->fetchOne($select, $bind);
        if ($result) {
            throw new MSTS_InvoiceMe_NotUniqueClientReferenceIdException();
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return MSTS_InvoiceMe_Model_Observer
     */
    public function makePaymentMethodAvailableIfModuleFullyConfigured($observer)
    {
        /** @var Mage_Payment_Model_Method_Abstract $methodInstance */
        $methodInstance = $observer->getMethodInstance();
        if (MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE != $methodInstance->getCode()) {
            return $this;
        }

        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        if (!$helper->validateModuleFullyConfigured()) {
            $result = $observer->getResult();
            $result->isAvailable = false;
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return MSTS_InvoiceMe_Model_Observer
     */
    public function makePaymentMethodAvailableToProperUsers($observer)
    {
        /** @var Mage_Payment_Model_Method_Abstract $methodInstance */
        $methodInstance = $observer->getMethodInstance();
        if (MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE != $methodInstance->getCode()) {
            return $this;
        }

        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');
        if (MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::AVAILABILITY_ALL_CUSTOMERS == $helper->getAvailability()) {
            return $this;
        }

        $result = $observer->getResult();
        /** @var Mage_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $customer = $customerSession->getCustomer();
        if (!$customer || !$customer->getId()) {
            $result->isAvailable = false;
        }

        /** @var MSTS_InvoiceMe_Helper_Customer $invoiceMeCustomerHelper */
        $invoiceMeCustomerHelper = Mage::helper('msts_invoiceme/customer');
        if (MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_ACTIVE != $invoiceMeCustomerHelper->getBuyerStatus($customer)) {
            $result->isAvailable = false;
        }

        return $this;
    }
}
