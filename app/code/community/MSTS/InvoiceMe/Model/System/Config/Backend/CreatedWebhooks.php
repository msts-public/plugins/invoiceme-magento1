<?php

class MSTS_InvoiceMe_Model_System_Config_Backend_CreatedWebhooks extends Mage_Core_Model_Config_Data
{

    /**
     * Mask webhook auth token value
     *
     * @inheritdoc
     */
    protected function _afterLoad()
    {
        $value = (string)$this->getValue();
        if (!empty($value) && ($webhooksData = Mage::helper('core')->jsonDecode($value))) {
            /** @var MSTS_InvoiceMe_Model_Webhook $webhook */
            $webhook = Mage::getModel('msts_invoiceme/webhook');

            $webhook->maskWebhooksCreatedValues($webhooksData);
            $this->setValue(Mage::helper('core')->jsonEncode($webhooksData));
        }
    }
}
