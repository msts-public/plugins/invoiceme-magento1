<?php

class MSTS_InvoiceMe_Model_System_Config_Source_Availability
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::AVAILABILITY_ALL_CUSTOMERS,
                'label' => Mage::helper('msts_invoiceme')->__('All customers')
            ],
            [
                'value' => MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::AVAILABILITY_ACTIVE_BUYERS_ONLY,
                'label' => Mage::helper('msts_invoiceme')->__('Active buyers only')
            ],
        ];
    }
}
