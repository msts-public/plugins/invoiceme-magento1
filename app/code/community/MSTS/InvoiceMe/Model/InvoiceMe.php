<?php

class MSTS_InvoiceMe_Model_InvoiceMe extends Varien_Object
{

    /**
     * @param Mage_Customer_Model_Customer $customer
     */
    public function processOrders(Mage_Customer_Model_Customer $customer)
    {
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        $ordersCollection = $this->getPendingOrders($customer);
        if ($ordersCollection) {
            foreach ($ordersCollection as $order) {
                /** @var Mage_Sales_Model_Order $order */
                try {
                    $comment = '';
                    $paymentAction = $helper->getPaymentAction();
                    switch ($paymentAction) {
                        case Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE:
                            $comment = $helper->__("InvoiceMe authorized amount of %s.",
                                Mage::helper('core')->currencyByStore($order->getPayment()->getAmountOrdered(), $order->getStore(), true, false)
                            );
                            break;
                        case Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE:
                            $comment = $helper->__("InvoiceMe captured amount of %s.",
                                Mage::helper('core')->currencyByStore($order->getPayment()->getAmountOrdered(), $order->getStore(), true, false)
                            );
                            break;
                    }

                    if ($this->processOrder($customer, $order, $paymentAction)) {
                        $notifyCustomer = true;
                        $order->setState(
                            Mage_Sales_Model_Order::STATE_PROCESSING,
                            Mage_Sales_Model_Order::STATE_PROCESSING,
                            $comment,
                            $notifyCustomer
                        );
                        $order->save();
                        $order->sendOrderUpdateEmail($notifyCustomer, $comment);
                    }
                } catch (Mage_Core_Exception $e) {
                    $order->addStatusHistoryComment(
                        $helper->__("Preauth error: %s", $e->getMessage())
                    )->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                    $order->addStatusHistoryComment(
                        $helper->__("Preauth core error: %s", $e->getMessage())
                    )->save();
                }
            }
        }
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param Mage_Sales_Model_Order $order
     * @param string $paymentAction
     * @return bool
     *
     * @throws Mage_Core_Exception
     */
    public function processOrder(Mage_Customer_Model_Customer $customer, Mage_Sales_Model_Order $order, $paymentAction)
    {
        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');

        if ($customer->getId() != $order->getCustomerId()) {
            Mage::throwException($helper->__('Order is not associated with the provided customer.'));
        }

        if (!$customer->getMstsImBuyerId()) {
            Mage::throwException($helper->__('Customer does not have the buyer ID assigned.'));
        }

        $payment = $order->getPayment();
        if (!$payment) {
            Mage::throwException($helper->__('Order has no payment method.'));
        }

        /** @var MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe $method */
        $method = $payment->getMethodInstance();
        if (!($method instanceof MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe)) {
            Mage::throwException($helper->__('Order payment method is not InvoiceMe.'));
        }

        /* In case of errors authorize and capture will throw exceptions */
        switch ($paymentAction) {
            case Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE:
                $method->authorize($payment, $payment->getAmountOrdered());

                if ($payment->getTransactionAdditionalInfo('status') == MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::PREAUTHORIZATION_RESPONSE_STATUS_PREAUTHORIZED) {
                    return true;
                }
                break;
            case Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE:
                $invoicesCollection = $order->getInvoiceCollection();
                foreach ($invoicesCollection as $invoice) {
                    /** @var Mage_Sales_Model_Order_Invoice $invoice */
                    if ($invoice->getState() != Mage_Sales_Model_Order_Invoice::STATE_OPEN) {
                        continue;
                    }

                    $invoice->capture()->save();
                }

                if ($payment->getTransactionAdditionalInfo('status') == MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::CHARGE_RESPONSE_STATUS_CREATED) {
                    return true;
                }
                break;
        }

        return false;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Sales_Model_Resource_Order_Collection|null
     */
    protected function getPendingOrders(Mage_Customer_Model_Customer $customer)
    {
        if (!$customer || !$customer->getId()) {
            return null;
        }

        /** @var Mage_Sales_Model_Resource_Order_Collection $ordersCollection */
        $ordersCollection = Mage::getModel('sales/order')->getCollection();
        $ordersCollection->addFieldToFilter('customer_id', $customer->getId());
        $ordersCollection->addFieldToFilter('status', MSTS_InvoiceMe_Helper_Data::ORDER_STATUS_PENDING_INVOICEME);
        return $ordersCollection;
    }

}