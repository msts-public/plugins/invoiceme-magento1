<?php

class MSTS_InvoiceMe_Model_Adminhtml_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     * @return MSTS_InvoiceMe_Model_Adminhtml_Observer
     */
    public function removeCreditMemoButtonFromOrderView($observer)
    {
        $block = $observer->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            /** @var Mage_Sales_Model_Order_Payment $payment */
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::registry('sales_order');
            if ($order && $order->getId() && $payment = $order->getPayment()) {
                if ($payment->getMethod() != MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE) {
                    return $this;
                }

                /** @var Mage_Adminhtml_Block_Sales_Order_View $block */
                $block->removeButton('order_creditmemo');
            }
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return MSTS_InvoiceMe_Model_Adminhtml_Observer
     */
    public function blockInvoiceActionIfPreauthorizationIsCancelled($observer)
    {
        $block = $observer->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            /** @var Mage_Sales_Model_Order_Payment $payment */
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::registry('sales_order');
            if ($order && $order->getId() && $payment = $order->getPayment()) {
                if ($payment->getMethod() != MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE
                    || $order->canVoidPayment()) {
                    return $this;
                }

                /** @var Mage_Adminhtml_Block_Sales_Order_View $block */
                $block->removeButton('order_invoice');
            }
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return MSTS_InvoiceMe_Model_Adminhtml_Observer
     */
    public function displayAdditionalFeeMessage($observer)
    {
        /** @var Mage_Adminhtml_Model_Session_Quote $quoteSession */
        $quoteSession = Mage::getSingleton('adminhtml/session_quote');
        $orderId = $quoteSession->getOrderId();
        if (!$orderId) {
            return $this;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);
        if (MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE == $order->getPayment()->getMethod()) {
            /** @var MSTS_InvoiceMe_Helper_Data $helper */
            $helper = Mage::helper('msts_invoiceme');
            Mage::getSingleton('adminhtml/session')->addWarning($helper->__('Please notice the InvoiceMe payment method was used for the edited order. After editing the order a totally new payment transaction will be created and as a Seller you will be charged with fees as for any payment transaction.'));
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return MSTS_InvoiceMe_Model_Adminhtml_Observer
     */
    public function displayPartialInvoiceNotPossibleMessage($observer)
    {
        $orderId = Mage::app()->getRequest()->getParam('order_id');
        if ($orderId) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId()
                && MSTS_InvoiceMe_Model_Payment_Method_InvoiceMe::METHOD_CODE == $order->getPayment()->getMethod()) {
                /** @var MSTS_InvoiceMe_Helper_Data $helper */
                $helper = Mage::helper('msts_invoiceme');
                if ($order->getBaseGiftCardsAmount() > 0) {
                    Mage::getSingleton('adminhtml/session')->addWarning(
                        $helper->__('Partial invoice is not possible because a Gift Card had been used.')
                    );
                }

                if ($order->getBaseCustomerBalanceAmount() > 0) {
                    Mage::getSingleton('adminhtml/session')->addWarning(
                        $helper->__('Partial invoice is not possible because Store Credits had been used.')
                    );
                }
            }
        }

        return $this;
    }
}
