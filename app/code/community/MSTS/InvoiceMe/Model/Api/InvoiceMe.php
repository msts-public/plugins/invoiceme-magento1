<?php

/**
 * @method string getApiKey()
 * @method MSTS_InvoiceMe_Model_Api_InvoiceMe setApiKey(string $apiKey)
 * @method string getPathIdParam()
 * @method MSTS_InvoiceMe_Model_Api_InvoiceMe setPathIdParam(string $pathIdParam)
 */
class MSTS_InvoiceMe_Model_Api_InvoiceMe extends Varien_Object
{
    const API_URL_PATH = 'api/v20180807/';

    const API_PATH_BUYERS            = 'buyers';
    const API_PATH_CHARGES           = 'charges';
    const API_PATH_PREAUTHORIZATIONS = 'preauthorizations';
    const API_PATH_WEBHOOKS          = 'webhooks';

    /** @var array */
    protected $createAPreauthorizationRequest = [
        'seller_id',
        'buyer_id',
        'currency',
        'preauthorized_amount',
    ];

    /** @var array */
    protected $createAPreauthorizationResponse = [
        'id',
        'seller_id',
        'buyer_id',
        'currency',
        'preauthorized_amount',
        'captured_amount',
        'foreign_exchange_fee',
        'status',
        'po_number',
        'expires',
        'created',
        'modified',
    ];

    /** @var array */
    protected $createAChargeRequest = [
        'seller_id',
        'buyer_id',
        'currency',
        'total_amount',
        'tax_amount',
        'discount_amount',
        'shipping_amount',
        'shipping_tax_amount',
        'shipping_discount_amount',
        'order_url',
        'order_number',
        'po_number',
        'preauthorization_id',
        'previous_charge_id',
        'comment',
        'details' => [
            'sku',
            'description',
            'quantity',
            'unit_price',
            'tax_amount',
            'discount_amount',
            'subtotal',
        ],
        'ship_to' => [
            'recipient_name',
            'company_address' => [
                'address_line1',
                'address_line2',
                'country',
                'city',
                'state',
                'zip',
            ],
            'pickup',
            'tracking' => [
                'tracking_number',
                'tracking_url',
                'carrier',
            ],
        ],
    ];

    /** @var array */
    protected $commonChargeResponse = [
        'id',
        'seller_id',
        'buyer_id',
        'status',
        'currency',
        'total_amount',
        'tax_amount',
        'discount_amount',
        'shipping_amount',
        'shipping_tax_amount',
        'shipping_discount_amount',
        'foreign_exchange_fee',
        'original_total_amount',
        'paid_amount_currency',
        'paid_amount',
        'order_url',
        'order_number',
        'po_number',
        'previous_charge_id',
        'comment',
        'ship_to',
        'recipient_name',
        'company_name',
        'company_address',
        'address_line1',
        'address_line2',
        'country',
        'city',
        'state',
        'zip',
        'pickup',
        'tracking',
        'tracking_number',
        'tracking_url',
        'carrier',
        'due_date',
        'created',
        'modified',
        'details',
        'sku',
        'description',
        'quantity',
        'unit_price',
        'tax_amount',
        'cancellation_reason',
        'return_reason',
    ];

    /** @var array */
    protected $cancelAChargeRequest = [
        'id',
        'reason',
    ];

    /** @var array */
    protected $returnAChargeRequest = [
        'id',
        'return_amount',
        'total_amount',
        'tax_amount',
        'discount_amount',
        'shipping_amount',
        'shipping_tax_amount',
        'shipping_discount_amount',
        'details' => [
            'sku',
            'description',
            'quantity',
            'unit_price',
            'tax_amount',
            'discount_amount',
            'subtotal',
        ],
        'return_reason',
        'return_comment',
    ];

    /** @var array */
    protected $getsTheStatusAndCreditInformationForABuyerResponse = [
        'id',
        'business_name',
        'client_reference_id',
        'status',
        'currency',
        'credit_approved',
        'credit_balance',
        'credit_preauthorized',
    ];

    /** @var array */
    protected $createAWebhookRequest = [
        'webhook_url',
        'webhook_auth_token',
        'webhook_auth_token_header',
        'event_types',
    ];

    /** @var array */
    protected $createAWebhookResponse = [
        'id',
        'webhook_url',
        'webhook_auth_token',
        'webhook_auth_token_header',
        'event_types',
        'created',
        'modified',
    ];

    /**
     * Callbacks for preparing values to API request
     *
     * @var array
     */
    protected $exportToRequestCallbacks = [
        'preauthorized_amount'      => 'amountToSubunits',
        'total_amount'              => 'amountToSubunits',
        'tax_amount'                => 'amountToSubunits',
        'discount_amount'           => 'amountToSubunits',
        'shipping_amount'           => 'amountToSubunits',
        'shipping_tax_amount'       => 'amountToSubunits',
        'shipping_discount_amount'  => 'amountToSubunits',
        'unit_price'                => 'amountToSubunits',
        'subtotal'                  => 'amountToSubunits',
        'return_amount'             => 'amountToSubunits',
    ];

    /**
     * Callbacks for preparing values from API response
     *
     * @var array
     */
    protected $importFromResponseCallbacks = [
        'preauthorized_amount'      => 'amountFromSubunits',
        'captured_amount'           => 'amountFromSubunits',
        'credit_approved'           => 'amountFromSubunits',
        'credit_balance'            => 'amountFromSubunits',
        'credit_preauthorized'      => 'amountFromSubunits',
        'original_total_amount'     => 'amountFromSubunits',
        'paid_amount'               => 'amountFromSubunits',
        'total_amount'              => 'amountFromSubunits',
        'tax_amount'                => 'amountFromSubunits',
        'discount_amount'           => 'amountFromSubunits',
        'shipping_amount'           => 'amountFromSubunits',
        'shipping_tax_amount'       => 'amountFromSubunits',
        'shipping_discount_amount'  => 'amountFromSubunits',
        'unit_price'                => 'amountFromSubunits',
        'subtotal'                  => 'amountFromSubunits',
    ];

    /**
     * The following values will be masked in all requests and responses when stored in the debug log
     *
     * @var array
     */
    protected $dataToBeMaskedForDebug = [
        self::API_PATH_PREAUTHORIZATIONS => [
            'seller_id',
            'buyer_id',
        ],
        self::API_PATH_CHARGES => [
            'seller_id',
            'buyer_id',
        ],
        self::API_PATH_BUYERS => [
            'id',
        ],
        self::API_PATH_WEBHOOKS => [
            'webhook_auth_token',
        ]
    ];

    /** @var int */
    protected $successResponseHttpStatusCode = 200;

    /** @var MSTS_InvoiceMe_Helper_Data */
    protected $helper = null;

    /**
     * @return MSTS_InvoiceMe_Helper_Data
     */
    protected function getHelper()
    {
        if (null === $this->helper) {
            $this->helper = Mage::helper('msts_invoiceme');
        }

        return $this->helper;
    }

    /**
     * @param string $methodName
     * @param bool $debug
     * @return string
     * @throws Exception
     */
    public function getApiEndpoint($methodName, $debug = false)
    {
        $apiUrl = $this->getHelper()->getApiUrl();
        if (!$apiUrl) {
            throw new Exception('API URL cannot be empty.');
        }

        $apiEndpoint = $apiUrl . self::API_URL_PATH . $methodName;
        if ($id = $this->getPathIdParam()) {
            if ($debug && self::API_PATH_BUYERS === $methodName) {
                $id = $this->getHelper()->maskValue($id);
            }

            $apiEndpoint .= '/' . $id;
            if (self::API_PATH_BUYERS === $methodName) {
                $apiEndpoint .= '/status';
            }
        }

        return $apiEndpoint;
    }

    /**
     * @return string
     */
    public function getAuthorizationHeaderValue()
    {
        $apiKey = $this->getApiKey();
        if (!$apiKey) {
            $apiKey = $this->getHelper()->getApiKey();
        }

        return 'Bearer ' . $apiKey;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callCreateAPreauthorization()
    {
        $this->setSuccessResponseHttpStatusCode(201);
        $request = $this->exportToRequest($this->createAPreauthorizationRequest);
        $response = $this->call(
            'Create a preauthorization',
            self::API_PATH_PREAUTHORIZATIONS,
            Zend_Http_Client::POST,
            $request
        );
        $this->processResponse($this->createAPreauthorizationResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callCancelAPreauthorization()
    {
        $response = $this->call('Cancel a charge', self::API_PATH_PREAUTHORIZATIONS, Zend_Http_Client::DELETE);
        $this->processResponse($this->createAPreauthorizationResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callCreateACharge()
    {
        $this->setSuccessResponseHttpStatusCode(201);
        $request = $this->exportToRequest($this->createAChargeRequest);
        $response = $this->call('Create a charge', self::API_PATH_CHARGES, Zend_Http_Client::POST, $request);
        $this->processResponse($this->commonChargeResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callCancelACharge()
    {
        $request = $this->exportToRequest($this->cancelAChargeRequest);
        $response = $this->call('Cancel a charge', self::API_PATH_CHARGES, Zend_Http_Client::DELETE, $request);
        $this->processResponse($this->commonChargeResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callReturnACharge()
    {
        $this->setSuccessResponseHttpStatusCode(201);
        $request = $this->exportToRequest($this->returnAChargeRequest);
        $response = $this->call('Return a charge', self::API_PATH_CHARGES, Zend_Http_Client::POST, $request);
        $this->processResponse($this->commonChargeResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callGetsTheStatusAndCreditInformationForABuyer()
    {
        $response = $this->call('Gets the status and credit information for a buyer', self::API_PATH_BUYERS);
        $this->processResponse($this->getsTheStatusAndCreditInformationForABuyerResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callCreateAWebhook()
    {
        $this->setSuccessResponseHttpStatusCode(201);
        $request = $this->exportToRequest($this->createAWebhookRequest);
        $response = $this->call('Create a webhook', self::API_PATH_WEBHOOKS, Zend_Http_Client::POST, $request);
        $this->processResponse($this->createAWebhookResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callListWebhooks()
    {
        $response = $this->call('List webhooks', self::API_PATH_WEBHOOKS);
        $this->processResponse($this->createAWebhookResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Api_InvoiceMe
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function callDeleteAWebhook()
    {
        $response = $this->call('Delete a webhook', self::API_PATH_WEBHOOKS, Zend_Http_Client::DELETE);
        $this->processResponse($this->createAWebhookResponse, $response);
        $this->importFromResponse($response);

        return $this;
    }

    /**
     * @param int $code
     */
    protected function setSuccessResponseHttpStatusCode($code)
    {
        $this->successResponseHttpStatusCode = $code;
    }

    /**
     * Export $this public data to private request array
     *
     * @param array $privateRequestMap
     * @param array $data
     * @param bool|string $currency
     * @return array
     */
    protected function exportToRequest(array $privateRequestMap, array $data = null, $currency = false)
    {
        $data = $data ?: $this->getData();
        if (!$currency) {
            $currency = $this->extractCurrency($data);
        }

        $result = [];
        foreach ($privateRequestMap as $index => $key) {
            if (is_array($key)) {
                if (isset($data[$index]) && is_array($data[$index])) {
                    $result[$index] = [];
                    foreach ($data[$index] as $subKey => $value) {
                        if (is_array($value)) {
                            if (is_array($key[$subKey])) {
                                $result[$index][$subKey] = $this->exportToRequest($key[$subKey], $value, $currency);
                            } else {
                                $result[$index][] = $this->exportToRequest($key, $value, $currency);
                            }
                        } else {
                            $result[$index][$subKey] = $value;
                        }
                    }
                }
            } elseif (isset($data[$key])) {
                $result[$key] = $data[$key];
                if (isset($this->exportToRequestCallbacks[$key]) && isset($result[$key])) {
                    $callback = $this->exportToRequestCallbacks[$key];
                    if ('amountToSubunits' == $callback) {
                        $result[$key] = call_user_func([$this, $callback], $result[$key], $currency);
                    } else {
                        $result[$key] = call_user_func([$this, $callback], $result[$key]);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param array $privateResponseMap
     * @param array $response
     * @param bool|string $currency
     */
    protected function processResponse(array $privateResponseMap, array &$response, $currency = false)
    {
        if (!$currency) {
            $currency = $this->extractCurrency($response);
        }

        foreach ($response as $key => $value) {
            if (is_array($value)) {
                $this->processResponse($privateResponseMap, $response[$key], $currency);
            } else {
                if (in_array($key, $privateResponseMap)) {
                    if (isset($this->importFromResponseCallbacks[$key])) {
                        $callback = $this->importFromResponseCallbacks[$key];
                        if ('amountFromSubunits' == $callback) {
                            $response[$key] = call_user_func([$this, $callback], $value, $currency);
                        } else {
                            $response[$key] = call_user_func([$this, $callback], $value);
                        }
                    }
                } else {
                    unset($response[$key]);
                }
            }
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function extractCurrency(array $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $result = $this->extractCurrency($value);
                if ($result) {
                    return $result;
                }
            } elseif ('currency' == $key) {
                return $value;
            }
        }

        return false;
    }

    /**
     * Import $this public data from a private response array
     *
     * @param array $response
     */
    protected function importFromResponse(array $response)
    {
        $isResponseAnArray = false;
        if (count(array_filter(array_keys($response), 'is_string')) === 0) {
            $isResponseAnArray = true;
        }

        if ($isResponseAnArray) {
            $this->setResponse($response);
        } else {
            foreach ($response as $key => $value) {
                $this->setDataUsingMethod($key, $value);
            }
        }
    }

    /**
     * Do the API call
     *
     * @param string $methodNameForDebug
     * @param string $methodName
     * @param string $httpMethod
     * @param array $request
     * @return array
     * @throws MSTS_InvoiceMe_ApiErrorResponseException
     * @throws Exception
     */
    public function call($methodNameForDebug, $methodName, $httpMethod = Zend_Http_Client::GET, array $request = [])
    {
        $apiEndpoint = $this->getApiEndpoint($methodName);
        $requestDebug = $request;
        $this->maskValues($requestDebug, $methodName);
        $debugData = [
            'type'                  => "call to the MSTS API ($methodNameForDebug)",
            'request_url'           => $this->getApiEndpoint($methodName, true),
            'request_http_method'   => $httpMethod,
            'request_auth_header'   => 'Authorization: Bearer '
                . $this->getHelper()->maskValue(str_replace('Bearer ', '', $this->getAuthorizationHeaderValue())),
            'request'               => $requestDebug,
        ];

        try {
            $http = new Zend_Http_Client();
            $config = [
                'maxredirects'  => 0,
                'timeout'       => 30,
            ];

            $http->setUri($apiEndpoint);
            $http->setConfig($config);
            $http->setHeaders('Authorization', $this->getAuthorizationHeaderValue());
            if ($httpMethod == Zend_Http_Client::DELETE && !empty($request)) {
                $http->setHeaders('Content-Type', 'application/json');
            }

            $http->setMethod($httpMethod);
            $this->buildAndAddBodyPayloadToTheHttpRequest($http, $request);

            $response = $http->request();
        } catch (Exception $e) {
            $debugData['http_error'] = ['error' => $e->getMessage(), 'code' => $e->getCode()];
            $this->logDebug($debugData);
            throw $e;
        }

        $result = Mage::helper('core')->jsonDecode($response->getBody());
        $resultDebug = $result;
        $this->maskValues($resultDebug, $methodName);
        $debugData['response_http_code']    = $response->getStatus();
        $debugData['response_raw']          = Mage::helper('core')->jsonEncode($resultDebug);
        $debugData['response']              = $resultDebug;
        $this->logDebug($debugData);

        $storeName = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
        $storePhone = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_PHONE);
        $storeGeneralEmail = Mage::getStoreConfig('trans_email/ident_general/email');
        if ($storeName) {
            if ($storePhone && $storeGeneralEmail) {
                $genericErrorMessage = $this->getHelper()->__(
                    'We are sorry! An error has occurred and we want to fix it. Please contact %s at %s or %s',
                    $storeName,
                    $storePhone,
                    $storeGeneralEmail
                );
            } else {
                $genericErrorMessage = $this->getHelper()->__(
                    'We are sorry! An error has occurred and we want to fix it. Please contact %s.',
                    $storeName
                );
            }
        } else {
            if ($storePhone && $storeGeneralEmail) {
                $genericErrorMessage = $this->getHelper()->__(
                    'We are sorry! An error has occurred and we want to fix it. Please contact us at %s or %s',
                    $storePhone,
                    $storeGeneralEmail
                );
            } else {
                $genericErrorMessage = $this->getHelper()->__(
                    'We are sorry! An error has occurred and we want to fix it. Please contact us.'
                );
            }
        }

        if ($response->isError()) {
            $code = isset($result['code']) ? $result['code'] : '';
            $message = isset($result['message']) ? $result['message'] : $response;
            Mage::logException(
                new MSTS_InvoiceMe_ApiErrorResponseException(
                    sprintf('InvoiceMe http connection error #%s: %s (%s)', $response->getStatus(), $code, $message)
                )
            );

            $apiErrorResponseException = new MSTS_InvoiceMe_ApiErrorResponseException(
                $genericErrorMessage,
                $response->getStatus()
            );
            $apiErrorResponseException->setApiErrorCode($code);
            throw $apiErrorResponseException;
        }

        if (!$this->validateResponse($response)) {
            Mage::logException(
                new MSTS_InvoiceMe_ApiErrorResponseException(
                    sprintf("Response http status code does not match the API specification. The expected http status code is %s, received %s", $this->successResponseHttpStatusCode, $response->getStatus())
                )
            );

            throw new MSTS_InvoiceMe_ApiErrorResponseException($genericErrorMessage);
        }

        return $result;
    }

    /**
     * @param float $value
     * @param string $currency
     * @return int
     */
    public function amountToSubunits($value, $currency)
    {
        return $this->getHelper()->amountToSubunits($value, $currency);
    }

    /**
     * @param int|string $value
     * @param string $currency
     * @return float
     */
    public function amountFromSubunits($value, $currency)
    {
        return $this->getHelper()->amountFromSubunits($value, $currency);
    }

    /**
     * @param array $data
     * @param string $methodName
     */
    protected function maskValues(&$data, $methodName)
    {
        if (!array_key_exists($methodName, $this->dataToBeMaskedForDebug)) {
            return;
        }

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $this->maskValues($data[$key], $methodName);
            } else {
                if (in_array($key, $this->dataToBeMaskedForDebug[$methodName], true)) {
                    if ('webhook_auth_token' === $key) {
                        $data[$key] = 'Bearer ' .  $this->getHelper()->maskValue(str_replace('Bearer ', '', $value));
                    } else {
                        $data[$key] = $this->getHelper()->maskValue($value);
                    }
                }
            }
        }
    }

    /**
     * @param Zend_Http_Client $http
     * @param array $request
     */
    protected function buildAndAddBodyPayloadToTheHttpRequest($http, $request)
    {
        if (!empty($request)) {
            $http->setRawData(Mage::helper('core')->jsonEncode($request), 'application/json');
        }
    }

    /**
     * @param Zend_Http_Response $response
     * @return bool
     */
    protected function validateResponse($response)
    {
        return $this->successResponseHttpStatusCode == $response->getStatus();
    }

    /**
     * @param mixed $debugData
     */
    protected function logDebug($debugData)
    {
        $this->getHelper()->debug($debugData);
    }
}
