<?php

class MSTS_InvoiceMe_Adminhtml_Customer_InvoiceMeController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @param string $idFieldName
     * @return $this
     */
    protected function initCustomer($idFieldName = 'id')
    {
        $customerId = (int) $this->getRequest()->getParam($idFieldName);
        $customer = Mage::getModel('customer/customer');

        if ($customerId) {
            $customer->load($customerId);
        }

        Mage::register('current_customer', $customer);
        return $this;
    }

    /**
     * Gets general information about a customer from buyer.status webhook
     */
    public function indexAction()
    {
        $this->initCustomer();
        $this->loadLayout()
            ->getLayout()
            ->getBlock('admin.customer.msts.invoiceme')
            ->initForm();
        $this->renderLayout();
    }

    /**
     * @inheritdoc
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('customer/invoiceme');
    }
}
