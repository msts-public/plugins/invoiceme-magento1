<?php

class MSTS_InvoiceMe_Adminhtml_System_Config_PaymentMethods_InvoiceMeController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Updates "Status of the webhooks" field
     *
     * @return void
     */
    public function checkWebhooksStatusAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return;
        }

        Mage::app()->getConfig()->reinit();

        $helper = $this->initHelper();
        $webhooksData = $helper->getCreatedWebhooks();

        $webhook = $this->initWebhook();
        $areAllWebhooksCreated = $webhook->validateAllWebhooksCreated(Mage::helper('core')->jsonDecode($webhooksData));
        $doesApiKeyForCreatedWebhooksMatchTheCurrentlyConfiguredApiKey = $helper->validateApiKeyForCreatedWebhooks();
        $doesBaseUrlForCreatedWebhooksMatchTheCurrentlyConfiguredBaseUrl = $helper->validateBaseUrlForCreatedWebhooks();

        $result = [];
        if ($areAllWebhooksCreated && $helper->isActive()) {
            $result['statusCreated'] = 'success';
            $result['messageCreated'] = $helper->__('Webhooks created');
            if ($doesApiKeyForCreatedWebhooksMatchTheCurrentlyConfiguredApiKey) {
                $result['statusApiKey'] = 'success';
                $result['messageApiKey'] = $helper->__('API key matching webhooks');
                if ($doesBaseUrlForCreatedWebhooksMatchTheCurrentlyConfiguredBaseUrl) {
                    $result['statusBaseUrl'] = 'success';
                    $result['messageBaseUrl'] = $helper->__('Base URL matching webhooks Base URL');
                    $result['tooltip'] = '';
                } else {
                    $result['statusBaseUrl'] = 'warning';
                    $result['messageBaseUrl'] = $helper->__('Base URL not matching webhooks Base URL');
                    if ('default' === $this->getRequest()->getParam('scope')) {
                        $result['tooltip'] = $helper->__('<p>It looks like the website <strong>Base URL</strong> had been edited recently.</p><p>Please review the settings and once you confirm everything is set up correctly, click on the <strong>Check Created Webhooks</strong> button. Magento will fetch the webhooks created in InvoiceMe according to the currently configured <strong>API Key</strong> (or <strong>Sandbox API Key</strong> if the <strong>Sandbox Mode</strong> is set to "Yes")</p><p>If webhooks are not created in InvoiceMe or they are created but using the old website <strong>Base URL</strong>, click on the <strong>(Re)Create Webhooks</strong> button. The webhooks will be re(created) according to the current InvoiceMe module\'s configuration.</p>');
                    } else {
                        $result['tooltip'] = $helper->__('<p>Please notice this might be correct because if your websites do not require separate InvoiceMe programs it is fine for webhooks to be triggered only to your main website URL</p><p>If you want to use multiple InvoiceMe programs, please review the settings and once you confirm everything is set up correctly, click on the <strong>Check Created Webhooks</strong> button. Magento will fetch the webhooks created in InvoiceMe according to the currently configured <strong>API Key</strong> (or <strong>Sandbox API Key</strong> if the <strong>Sandbox Mode</strong> is set to "Yes")</p><p>If webhooks are not created in InvoiceMe or they are created but using the old website <strong>Base URL</strong>, click on the <strong>(Re)Create Webhooks</strong> button. The webhooks will be re(created) according to the current InvoiceMe module\'s configuration.</p>');
                    }
                }
            } else {
                $result['statusApiKey'] = 'error';
                $result['messageApiKey'] = $helper->__('API key not matching webhooks API key');
                $result['tooltip'] = $helper->__('<p>Please review the settings and once you confirm everything is set up correctly, click on the <strong>Check Created Webhooks</strong> button. Magento will fetch the webhooks created in InvoiceMe according to the currently configured <strong>API Key</strong> (or <strong>Sandbox API Key</strong> if the <strong>Sandbox Mode</strong> is set to "Yes")</p><p>If webhooks are not created in InvoiceMe, click on the <strong>(Re)Create Webhooks</strong> button. The webhooks will be re(created) according to the current InvoiceMe module\'s configuration.</p>');
            }
        } elseif ($areAllWebhooksCreated) {
            $result['statusCreated'] = 'error';
            $result['messageCreated'] = $helper->__('Webhooks created but the module is disabled!');
            $result['tooltip'] = $helper->__('<p>If you plan not to use the InvoiceMe module any longer, please delete the created webhooks so the InvoiceMe will stop sending updates to your Magento website. In order to do it please click on the <strong>Delete Webhooks</strong> button.</p><p>Please notice that webhooks can be (re)created at any time by clicking on the <strong>(Re)Create Webhooks</strong> button.</p>');
        } else {
            $result['statusCreated'] = 'error';
            $result['messageCreated'] = $helper->__('Webhooks not created!');
            if ($helper->getApiKey()) {
                $result['tooltip'] = $helper->__('<p>It looks like the <strong>API Key</strong> had been edited recently or the <strong>Sandbox Mode</strong> status had been changed.</p><p>Please review the settings and once you confirm everything is set up correctly, click on the <strong>Check Created Webhooks</strong> button. Magento will fetch the webhooks created in InvoiceMe according to the currently configured <strong>API Key</strong> (or <strong>Sandbox API Key</strong> if the <strong>Sandbox Mode</strong> is set to "Yes")</p><p>If webhooks are not created in InvoiceMe, click on the <strong>(Re)Create Webhooks</strong> button. The webhooks will be re(created) according to the current InvoiceMe module\'s configuration.</p>');
            } else {
                $result['tooltip'] = $helper->__('<p>Please review the settings and once you confirm everything is set up correctly, click on the <strong>Check Created Webhooks</strong> button. Magento will fetch the webhooks created in InvoiceMe according to the currently configured <strong>API Key</strong> (or <strong>Sandbox API Key</strong> if the <strong>Sandbox Mode</strong> is set to "Yes")</p><p>If webhooks are not created in InvoiceMe, click on the <strong>(Re)Create Webhooks</strong> button. The webhooks will be re(created) according to the current InvoiceMe module\'s configuration.</p>');
            }
        }

        $this->sendAjaxResponse($result);
    }

    /**
     * Fetches webhooks created in the InvoiceMe
     *
     * @return void
     */
    public function checkCreatedWebhooksAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return;
        }

        $result = [];

        try {
            $webhook = $this->initWebhook();
            $webhook->deleteWebhooksIfApiKeyChanged();
            $webhooksData = $webhook->listWebhooks();

            $helper = $this->initHelper();
            $helper->updateCreatedWebhooks($webhooksData);

            $result['status'] = 'success';
            $webhook->maskWebhooksCreatedValues($webhooksData);
            $result['createdWebhooks'] = Mage::helper('core')->jsonEncode($webhooksData);
        } catch (MSTS_InvoiceMe_ApiErrorResponseException $e) {
            $this->updateResponseWithErrorMessage($e, $result);
        } catch (Exception $e) {
            $this->updateResponseWithErrorMessage($e, $result);
        }

        $this->sendAjaxResponse($result);
    }

    /**
     * Creates webhooks in the InvoiceMe. If there are any webhooks created already they will be deleted first.
     *
     * @return void
     */
    public function createWebhooksAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return;
        }

        $result = [];

        $helper = $this->initHelper();
        if ($helper->validateOpensslPhpExtensionLoaded()) {
            try {
                $webhook = $this->initWebhook();
                $webhook->deleteWebhooks(Mage::helper('core')->jsonDecode($helper->getCreatedWebhooks()));

                $webhooksData = $webhook->createWebhooks($helper->getBaseSecureUrl());
                $helper->updateCreatedWebhooks($webhooksData);

                $result['status'] = 'success';
                $webhook->maskWebhooksCreatedValues($webhooksData);
                $result['createdWebhooks'] = Mage::helper('core')->jsonEncode($webhooksData);
            } catch (MSTS_InvoiceMe_ApiErrorResponseException $e) {
                $this->updateResponseWithErrorMessage($e, $result);
            } catch (Exception $e) {
                $this->updateResponseWithErrorMessage($e, $result);
            }
        } else {
            $result['status'] = 'error';
            $result['message'] = $this->__('Required PHP extension \'openssl\' was not loaded.');
        }

        $this->sendAjaxResponse($result);
    }

    /**
     * Deletes webhooks in the InvoiceMe
     *
     * @return void
     */
    public function deleteWebhooksAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return;
        }

        $result = [];

        $helper = $this->initHelper();
        try {
            $webhook = $this->initWebhook();
            $webhooksData = Mage::helper('core')->jsonDecode($helper->getCreatedWebhooks());
            $webhook->deleteWebhooks($webhooksData);
            $helper->updateCreatedWebhooks([]);

            $result['status'] = 'success';
        } catch (MSTS_InvoiceMe_ApiErrorResponseException $e) {
            $this->updateResponseWithErrorMessage($e, $result);
        } catch (Exception $e) {
            $this->updateResponseWithErrorMessage($e, $result);
        }

        $this->sendAjaxResponse($result);
    }

    /**
     * @param Exception $e
     * @param array $result
     */
    protected function updateResponseWithErrorMessage($e, &$result)
    {
        $result['status'] = 'error';
        if ($e instanceof MSTS_InvoiceMe_ApiErrorResponseException) {
            $result['message'] = $this->__(
                'An error occurred with the following message: "%s". Please check the exception log for more details.',
                $e->getMessage()
            );
        } else {
            $result['message'] = $this->__(
                'An error occurred with the following message: "%s". Please enable the debug mode, try again and check the debug log for more details.',
                $e->getMessage()
            );
        }
    }

    /**
     * @return MSTS_InvoiceMe_Helper_Data
     */
    protected function initHelper()
    {
        $scope = $this->getRequest()->getParam('scope', 'default');
        $scopeId = $this->getRequest()->getParam('scopeId', 0);
        $scopeCode = $this->getRequest()->getParam('scopeCode', null);

        /** @var MSTS_InvoiceMe_Helper_Data $helper */
        $helper = Mage::helper('msts_invoiceme');
        $helper->setScope($scope);
        $helper->setScopeId($scopeId);
        $helper->setScopeCode($scopeCode);
        return $helper;
    }

    /**
     * @return MSTS_InvoiceMe_Model_Webhook
     */
    protected function initWebhook()
    {
        $scope = $this->getRequest()->getParam('scope', 'default');
        $scopeCode = $this->getRequest()->getParam('scopeCode', null);

        /** @var MSTS_InvoiceMe_Model_Webhook $webhook */
        $webhook = Mage::getModel('msts_invoiceme/webhook');
        $webhook->setScope($scope);
        $webhook->setScopeCode($scopeCode);
        return $webhook;
    }

    /**
     * Check is allowed access to action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/config');
    }

    /**
     * @param $data
     */
    protected function sendAjaxResponse($data)
    {
        $this->getResponse()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }

}
