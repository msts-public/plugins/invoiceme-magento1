<?php

class MSTS_InvoiceMe_WebhookController extends Mage_Core_Controller_Front_Action
{
    /** @var MSTS_InvoiceMe_Helper_Data */
    protected $helper = null;

    /** @var array */
    protected $buyerStatusWebhookRequiredDataKeys = [
        'id',
        'business_name',
        'client_reference_id',
        'status',
        'currency',
        'credit_approved',
        'credit_balance',
        'credit_preauthorized',
    ];

    /** @var array */
    protected $preauthorizationUpdatedWebhookRequiredDataKeys = [
        'id',
        'status',
        'currency',
        'preauthorized_amount',
        'captured_amount',
        'expires',
    ];

    /**
     * Action predispatch
     *
     * Check webhook Authorization Header
     */
    public function preDispatch()
    {
        parent::preDispatch();

        $helper = $this->getHelper();

        if (!$helper->validateModuleFullyConfigured()) {
            $this->norouteAction();
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }

        if (!$helper->validateOpensslPhpExtensionLoaded()) {
            $debugData = [
                'module_validation_error' => 'Required PHP extension \'openssl\' was not loaded.',
            ];
            $this->debug($debugData, false);
            $this->norouteAction();
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }

        $webhookAuthTokenForCreatedWebhooks = $helper->getWebhookAuthTokenForCreatedWebhooks();
        if (!$webhookAuthTokenForCreatedWebhooks || !$this->getRequest()->isPost()) {
            $this->norouteAction();
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }

        if (!$this->validateAuthorizationHeader($webhookAuthTokenForCreatedWebhooks)) {
            $this->setUnauthorizedErrorResponse($this->__('Unauthenticated access'));
            $this->debug([]);
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }
    }

    /**
     * @return MSTS_InvoiceMe_Helper_Data
     */
    protected function getHelper()
    {
        if (null === $this->helper) {
            $this->helper = Mage::helper('msts_invoiceme');
        }

        return $this->helper;
    }

    /**
     * @param string $webhookAuthTokenForCreatedWebhooks
     * @return bool
     * @throws Zend_Controller_Request_Exception
     */
    protected function validateAuthorizationHeader($webhookAuthTokenForCreatedWebhooks)
    {
        $authorizationHeader = $this->getRequest()->getHeader('Authorization');
        return $authorizationHeader === $webhookAuthTokenForCreatedWebhooks;
    }

    /**
     * 'buyer.status' webhook call from InvoiceMe
     */
    public function buyerStatusAction()
    {
        $authorizationHeader = $this->getRequest()->getHeader('Authorization');
        $request = $this->getRequest();
        $rawBody = $request->getRawBody();
        $jsonInputData = Mage::helper('core')->jsonDecode($rawBody);
        $jsonInputDataDebug = $jsonInputData;
        $this->maskBuyerStatusInputData($jsonInputDataDebug);
        $debugData = [
            'type'                  => 'webhook from the MSTS API',
            'request_webhook_type'  => MSTS_InvoiceMe_Model_Webhook::WEBHOOK_TYPE_BUYER_STATUS,
            'request_auth_header'   => 'Authorization: Bearer '
                . $this->getHelper()->maskValue(str_replace('Bearer ', '', $authorizationHeader)),
            'request_raw'           => Mage::helper('core')->jsonEncode($jsonInputDataDebug),
            'request'               => $jsonInputDataDebug,
        ];
        if (!$this->validateBuyerStatusJsonSchema($jsonInputData)) {
            $this->setErrorResponse(sprintf('Request body failed JSON schema validation'));
            $this->debug($debugData);
            return;
        }

        try {
            $customer = $this->initCustomer($jsonInputData['data']['client_reference_id']);
            if (!$customer || !$customer->getId()) {
                $this->setErrorResponse(
                    sprintf(
                        'Customer was not found for the specified \'client_reference_id\' = %s',
                        $jsonInputData['data']['client_reference_id']
                    )
                );
                $this->debug($debugData);
                return;
            }

            $oldStatusOptionId = $customer->getMstsImStatus();
            $this->updateCustomerData($customer, $jsonInputData['data']);
            $customer->save();

            $this->processOrdersPlacedAsNotRegisteredBuyer($customer, $jsonInputData, $oldStatusOptionId);
        } catch (Exception $e) {
            $this->setErrorResponse(
                sprintf('There was an error trying to update customer\'s data. The error message: %s', $e->getMessage())
            );
            $this->debug($debugData);
            return;
        }

        $this->setSuccessResponse(sprintf('Customer\'s data have been successfully updated'));
        $this->debug($debugData);
    }

    /**
     * @param array $data
     */
    protected function maskBuyerStatusInputData(&$data)
    {
        if (isset($data['data']) && isset($data['data']['id'])) {
            $data['data']['id'] = $this->getHelper()->maskValue($data['data']['id']);
        }
    }

    /**
     * @param mixed $jsonInputData
     * @return bool
     */
    protected function validateBuyerStatusJsonSchema($jsonInputData)
    {
        if (!isset($jsonInputData['event_type'])
            || MSTS_InvoiceMe_Model_Webhook::WEBHOOK_TYPE_BUYER_STATUS != $jsonInputData['event_type']
            || !isset($jsonInputData['data'])
            || !$this->validateRequiredDataKeys($jsonInputData['data'], $this->buyerStatusWebhookRequiredDataKeys)
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param array $inputData
     * @param array $requiredDataKeys
     * @return bool
     */
    protected function validateRequiredDataKeys($inputData, $requiredDataKeys)
    {
        foreach ($requiredDataKeys as $key) {
            if (!isset($inputData[$key])) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param array $jsonInputData
     * @param int|null $oldStatusOptionId
     */
    protected function processOrdersPlacedAsNotRegisteredBuyer($customer, $jsonInputData, $oldStatusOptionId)
    {
        if (MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_ACTIVE == $jsonInputData['data']['status']) {
            $newStatusOptionId = Mage::getResourceSingleton('customer/customer')
                ->getAttribute('msts_im_status')
                ->getSource()
                ->getOptionId($jsonInputData['data']['status']);
            if (null === $oldStatusOptionId || $oldStatusOptionId != $newStatusOptionId) {
                /** @var MSTS_InvoiceMe_Helper_Order $helper */
                $helper = Mage::helper('msts_invoiceme/order');
                $helper->processOrders($customer);
            }
        }
    }

    /**
     * @param $message
     * @throws Zend_Controller_Response_Exception
     */
    protected function setUnauthorizedErrorResponse($message)
    {
        $this->setResponse(401, $message);
    }

    /**
     * @param $message
     * @throws Zend_Controller_Response_Exception
     */
    protected function setErrorResponse($message)
    {
        $this->setResponse(400, $message);
    }

    /**
     * @param $message
     * @throws Zend_Controller_Response_Exception
     */
    protected function setSuccessResponse($message)
    {
        $this->setResponse(200, $message);
    }

    /**
     * @param int $responseCode
     * @param string $message
     * @throws Zend_Controller_Response_Exception
     */
    protected function setResponse($responseCode, $message)
    {
        $result = [];
        $result['message'] = $message;
        $this->getResponse()
            ->setHttpResponseCode($responseCode)
            ->setHeader('Content-type', 'application/json', true)
            ->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * @param string $clientReferenceId
     * @return Mage_Customer_Model_Customer
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function initCustomer($clientReferenceId)
    {
        /** @var MSTS_InvoiceMe_Helper_Customer $invoiceMeCustomerHelper */
        $invoiceMeCustomerHelper = Mage::helper('msts_invoiceme/customer');

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer');
        if ($customer->getSharingConfig()->isWebsiteScope()) {
            foreach (Mage::app()->getWebsites() as $website) {
                $customer->reset();
                $customer->setWebsiteId($website->getId());
                $customer = $invoiceMeCustomerHelper->loadCustomerByClientReferenceId($customer, $clientReferenceId);
                if ($customer && $customer->getId()) {
                    break;
                }
            }
        } else {
            $currentWebsiteId = Mage::app()->getStore()->getWebsiteId();
            $customer->setWebsiteId($currentWebsiteId);

            $customer = $invoiceMeCustomerHelper->loadCustomerByClientReferenceId($customer, $clientReferenceId);
        }

        return $customer;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param array $data
     */
    protected function updateCustomerData(Mage_Customer_Model_Customer $customer, $data)
    {
        $helper = $this->getHelper();
        $customer->setMstsImBuyerId(Mage::getModel('core/encryption')->encrypt($data['id']));
        $customer->setMstsImBusinessName($data['business_name']);

        $statusOptionId = Mage::getResourceSingleton('customer/customer')
            ->getAttribute('msts_im_status')
            ->getSource()
            ->getOptionId($data['status']);
        $customer->setMstsImStatus($statusOptionId);

        $customer->setMstsImCurrency($data['currency']);
        $customer->setMstsImCreditApproved($helper->amountFromSubunits($data['credit_approved'], $data['currency']));
        $customer->setMstsImCreditBalance($helper->amountFromSubunits($data['credit_balance'], $data['currency']));
        $customer->setMstsImCreditPreauthorized(
            $helper->amountFromSubunits($data['credit_preauthorized'], $data['currency'])
        );
    }

    /**
     * 'preauthorization.updated' webhook call from InvoiceMe
     */
    public function preauthorizationUpdatedAction()
    {
        $authorizationHeader = $this->getRequest()->getHeader('Authorization');
        $request = $this->getRequest();
        $rawBody = $request->getRawBody();
        $jsonInputData = Mage::helper('core')->jsonDecode($rawBody);
        $debugData = [
            'type'                  => 'webhook from the MSTS API',
            'request_webhook_type'  => MSTS_InvoiceMe_Model_Webhook::WEBHOOK_TYPE_PREAUTHORIZATION_UPDATED,
            'request_auth_header'   => 'Authorization: Bearer '
                . $this->getHelper()->maskValue(str_replace('Bearer ', '', $authorizationHeader)),
            'request_raw'           => $rawBody,
            'request'               => $jsonInputData,
        ];
        if (!$this->validatePreauthorizationUpdatedJsonSchema($jsonInputData)) {
            $this->setErrorResponse(sprintf('Request body failed JSON schema validation'));
            $this->debug($debugData);
            return;
        }

        try {
            /** @var Mage_Sales_Model_Order_Payment_Transaction $transaction */
            $transaction = $this->initTransaction($jsonInputData['data']['id']);
            if (!$transaction || !$transaction->getId()) {
                $this->setErrorResponse(
                    sprintf('Transaction was not found for the specified \'id\' = %s', $jsonInputData['data']['id'])
                );
                $this->debug($debugData);
                return;
            }

            $payment = $transaction->getOrderPaymentObject(true);
            if (!$payment || !$payment->getId()) {
                $this->setErrorResponse(sprintf('Transaction payment data was not found'));
                $this->debug($debugData);
                return;
            }

            $this->updatePaymentData($payment, $transaction, $jsonInputData);
            $payment->save();
            $transaction->save();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->setErrorResponse(
                sprintf(
                    'There was an error trying to update transaction\'s data. The error message: %s', $e->getMessage()
                )
            );
            $this->debug($debugData);
            return;
        }

        $this->setSuccessResponse(sprintf('Transaction\'s data have been successfully updated'));
        $this->debug($debugData);
    }

    /**
     * @param mixed $jsonInputData
     * @return bool
     */
    protected function validatePreauthorizationUpdatedJsonSchema($jsonInputData)
    {
        if (!isset($jsonInputData['event_type'])
            || MSTS_InvoiceMe_Model_Webhook::WEBHOOK_TYPE_PREAUTHORIZATION_UPDATED != $jsonInputData['event_type']
            || !isset($jsonInputData['data'])
            || !$this->validateRequiredDataKeys(
                $jsonInputData['data'],
                $this->preauthorizationUpdatedWebhookRequiredDataKeys
            )
            || !isset($jsonInputData['timestamp'])
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param Mage_Sales_Model_Order_Payment_Transaction $transaction
     * @param array $jsonInputData
     */
    protected function updatePaymentData(
        Mage_Sales_Model_Order_Payment $payment,
        Mage_Sales_Model_Order_Payment_Transaction $transaction,
        &$jsonInputData
    ) {
        if (!$transaction->getIsClosed()) {
            $amount = $this->getHelper()->amountFromSubunits(
                $jsonInputData['data']['preauthorized_amount'],
                $jsonInputData['data']['currency']
            );
            $payment->setBaseAmountAuthorized($amount);
            $payment->setAmountAuthorized($amount);
        }

        $transactionAdditionalInfoKeys = [
            'status',
            'currency',
            'preauthorized_amount',
            'captured_amount',
            'expires',
        ];

        foreach ($jsonInputData['data'] as $key => $value) {
            if (!in_array($key, $transactionAdditionalInfoKeys)) {
                continue;
            }

            if (in_array($key, ['preauthorized_amount', 'captured_amount'])) {
                $value = $this->getHelper()->amountFromSubunits($value, $jsonInputData['data']['currency']);
            }

            $transaction->setAdditionalInformation($key, $value);
        }

        $transaction->setAdditionalInformation('modified', $jsonInputData['timestamp']);
    }

    /**
     * @param string $transactionId
     * @return Mage_Sales_Model_Order_Payment_Transaction
     * @throws Mage_Core_Exception
     */
    protected function initTransaction($transactionId)
    {
        return Mage::getModel('sales/order_payment_transaction')->load($transactionId, 'txn_id');
    }

    /**
     * @param array $debugData
     * @param bool $includeResponse
     */
    protected function debug($debugData, $includeResponse = true)
    {
        $helper = $this->getHelper();
        if ($includeResponse) {
            $response = $this->getResponse();
            $debugData['response_http_code'] = $response->getHttpResponseCode();
            $debugData['response'] = Mage::helper('core')->jsonDecode($response->getBody());
        }

        $helper->debug($debugData);
    }
}
