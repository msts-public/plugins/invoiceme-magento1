<?php

class MSTS_InvoiceMe_IndexController extends MSTS_InvoiceMe_Controller_Front_Abstract
{
    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    /**
     * Default action
     */
    public function indexAction()
    {
        /** @var Mage_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $customer = $customerSession->getCustomer();

        if (MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_ACTIVE == $this->getBuyerStatus($customer)) {
            $this->fetchAndUpdateBuyerData($customer);
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($block = $this->getLayout()->getBlock('msts_invoiceme_account_dashboard_section')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }

        $this->getLayout()->getBlock('head')->setTitle($this->__('InvoiceMe'));
        $this->renderLayout();
    }

    /**
     * Redirect customer to the InvoiceMe credit application form
     */
    public function applyForCreditAction()
    {
        /** @var Mage_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $customer = $customerSession->getCustomer();

        if ($this->getBuyerStatus($customer)) {
            $this->_redirect('*/*/index');
            return;
        }

        $applyForCreditUrl = $this->buildApplyForCreditUrl('*/*/index', 'msts_invoiceme', true);
        if ($applyForCreditUrl) {
            $this->_redirectUrl($applyForCreditUrl);
        }

        return;
    }
}
