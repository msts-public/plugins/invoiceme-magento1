<?php

class MSTS_InvoiceMe_GatewayController extends MSTS_InvoiceMe_Controller_Front_Abstract
{

    /**
     * Redirects customer to InvoiceMe credit application form and changes order status
     */
    public function redirectAction()
    {
        try {
            $orders = $this->getLastOrdersFromCheckoutSession();
            if ($orders) {
                try {
                    foreach ($orders as $order) {
                        /** @var Mage_Sales_Model_Order $order */
                        $order->setState(
                            Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW,
                            MSTS_InvoiceMe_Helper_Data::ORDER_STATUS_PENDING_INVOICEME,
                            $this->__('Customer redirected to the InvoiceMe credit application form.'),
                            false
                        );
                        $order->save();
                        $order->queueNewOrderEmail();
                    }
                } catch (Mage_Core_Exception $e) {
                    Mage::logException($e);
                }
            }

            $applyForCreditUrl = $this->buildApplyForCreditUrl('checkout/cart', 'msts_invoiceme');
            if ($applyForCreditUrl) {
                $this->_redirectUrl($applyForCreditUrl);
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('customer/session')->addError(
                $this->__('There was an error trying to apply for a credit.')
            );
            $this->_redirect('checkout/cart');
        }
    }

    /**
     * @return array
     */
    protected function getLastOrdersFromCheckoutSession()
    {
        $ids = Mage::getSingleton('core/session')->getOrderIds();
        if ($ids && is_array($ids)) {
            $this->setFlag('', 'is_multishipping', true);
        } else if (Mage::getSingleton('checkout/session')->getLastOrderId()) {
            $ids = [Mage::getSingleton('checkout/session')->getLastOrderId()];
        }

        $orders = [];
        if (is_array($ids)) {
            foreach ($ids as $id) {
                $order = Mage::getModel('sales/order')->load($id);
                if ($order && $order->getId()) {
                    $orders[] = $order;
                }
            }
        }

        return $orders;
    }

}
