<?php

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->addAttribute(
    'customer',
    'msts_im_business_name',
    [
        'type'     => 'varchar',
        'label'    => 'Business Name',
        'input'    => 'text',
        'visible'  => false,
        'required' => false,
    ]
);

$installer->addAttribute(
    'customer',
    'msts_im_status',
    [
        'type'     => 'int',
        'label'    => 'Status',
        'input'    => 'select',
        'source'   => 'eav/entity_attribute_source_table',
        'visible'  => false,
        'required' => false,
        'option'   => [
            'values' => [
                MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_ACTIVE,
                MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_CANCELLED,
                MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_DECLINED,
                MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_INACTIVE,
                MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING,
                MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING_DIRECT_DEBIT,
                MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_PENDING_SETUP,
                MSTS_InvoiceMe_Helper_Customer::BUYER_STATUS_SUSPENDED,
            ]
        ],
    ]
);

$installer->addAttribute(
    'customer',
    'msts_im_currency',
    [
        'type'     => 'varchar',
        'label'    => 'Currency',
        'input'    => 'text',
        'visible'  => false,
        'required' => false,
    ]
);

$installer->addAttribute(
    'customer',
    'msts_im_credit_approved',
    [
        'type'     => 'decimal',
        'label'    => 'Credit Approved',
        'input'    => 'text',
        'visible'  => false,
        'required' => false,
    ]
);

$installer->addAttribute(
    'customer',
    'msts_im_credit_balance',
    [
        'type'     => 'decimal',
        'label'    => 'Credit Balance',
        'input'    => 'text',
        'visible'  => false,
        'required' => false,
    ]
);

$installer->addAttribute(
    'customer',
    'msts_im_credit_preauthorized',
    [
        'type'     => 'decimal',
        'label'    => 'Credit Preauthorized',
        'input'    => 'text',
        'visible'  => false,
        'required' => false,
    ]
);

$installer->addAttribute(
    'customer',
    'msts_im_buyer_id',
    [
        'type'     => 'varchar',
        'label'    => 'Buyer ID',
        'input'    => 'hidden',
        'visible'  => false,
        'required' => false,
    ]
);

$installer->addAttribute(
    'customer',
    'msts_im_client_reference_id',
    [
        'type'     => 'static',
        'label'    => 'Client Reference ID',
        'input'    => 'text',
        'visible'  => false,
        'required' => false,
    ]
);

$installer->getConnection()->addColumn(
    $installer->getTable('customer/entity'),
    'msts_im_client_reference_id',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'nullable'  => true,
        'comment'   => 'Unique Customer Identifier'
    ]
);

$installer->getConnection()->addIndex(
    $installer->getTable('customer/entity'),
    $installer->getIdxName(
        'customer/entity',
        ['msts_im_client_reference_id', 'website_id']
    ),
    ['msts_im_client_reference_id', 'website_id']
);

$installer->getConnection()->addIndex(
    $installer->getTable('customer/entity'),
    $installer->getIdxName(
        'customer/entity',
        ['msts_im_client_reference_id', 'website_id'],
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    ['msts_im_client_reference_id', 'website_id'],
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->endSetup();
